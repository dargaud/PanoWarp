s��         zF   "  d�  �   �   ����                               Png                             ReadSavePng                                                                                              
�  	png_text[]     � ��png_text  �  � ��BOOL  	6    ReadSavePng for LabWindows/CVI
(c) Guillaume Dargaud - Free use and redistribution.
    Can be integrated in a commercial program,
    but must not be sold as a fp/lib/dll or source.

History:
1999/9/18, Original SaveAsPng functions
1999/12/18, Added ReadFromPng functions, added color and 2/8/24/32 bits conversion routines
2000/4/14, there was a bug for 16/24 bits graphic cards.
2001/2/8, Partial compatibility for Unix (just remove the two panel related functions). Added function to read the file parameters.
2001/4/15, Explosive bug in reading greyscale and 32 bit images corrected
2001/5/10, Use of bitmaps when reading now optional
2002/09/24, corrected error in reading greyscale files. Removed some buggy Alpha support.
2002/09/27, corrected error in reading palleted files.
2002/11/30, Added a function to read the file properties without reading the bitmap.

----------------------------------------------------------------
 
Save and read PNG (Portable Network Graphics) files.

Two pairs of functions are provided, one pair saves/reads a bitmap. The other pair saves a panel (or a control) or reads a file and puts it into a graphic control (like a canvas).

PNG files are compressed lossless graphic files (such as GIF) with number of colors ranging from 1 to 16 bits (2^(3*16)=281474976710656 colors or 18446744073709551616 with alpha), B&W, greyscale and alpha/transparency support.

Saving:
  Saving controls or panels:
    3*8 bit RGB resolution (with 24 bits monitors). 
    3*8 RGB+alpha (with 32 bits monitors).
  Saving direct bitmaps:
    Same as above, but also:
    8 bit greyscale
    1 bit black & white.
This FP file does not save paletted graphics (although support could be added).

Reading:
Any kind of PNG file can be read. They are converted to 3*8-bit RGB images without alpha channels before display in CVI.

PNG files can be displayed inline in browsers such as Netscape and Internet explorer above version 4. Use the <IMG> tag.

LIBPNG is a public domain source code that can be found on many web sites such as http://www.cdrom.com/pub/png/.

LIBPNG uses the ZLIB library that can be found at http://www.cdrom.com/pub/infozip/zlib/zlib.html.

----------------------------------------------------------------
For source code or assistance contact me at:
ouaibe@gdargaud.net
http://www.gdargaud.net/Hack/LabWindows.html
     �    Functions for reading PNG files.

Should be able to read any kind of PNG file, but will convert internally to 24 bits RGB or 8 bits greyscale.    R    Functions for saving PNG files.

Files saved can be: 
 - 3*8 bit RGB resolution if your monitor is in 16/24 bit mode,
 - 3*8 bit RGB (+8 bits Alpha if you specify the option) in 32 bit monitors.
 - 8 bits greyscale images
 - 1 bit B&W images
      
You cannot save paletted graphics, 16 bits per channel images or intermediate numbers.
     %    Functions for manipulating bitmaps.    +    Read a bitmap from a file.
This function can read any (?) kind of png file:
- paletted 2/4/16/256 colors
- paletted + Alpha channel
- 3*8 bit RGB (+Alpha channel)
- 3*16 bit RGB (+Alpha channel)
- 8/16 bits greyscale (+Alpha channel)

BUT:
- it will convert greyscale to RGB (except if Bitmap==NULL)
- it will strip the Alpha channel
So the returned Bits are 24 bit RGB, except in the case of a greyscale image with Null Bitmap where it is 8 bits greyscale.

You must free(Bits) and DiscardBitmap(Bitmap) after you are done using the (optional) bitmap.
     =    Name of the PNG file to save. Should have extension ".png".     #    Return TRUE if an error happened.    i    Pointer to a free bitmap.
After you are done using it, you must DiscardBitmap(Bitmap).

Note that if you pass null, a bitmap will not be allocated (saves a LOT of memory) but you will still receive the Bits arrays and the image parameters, which may be enough for some applications.

If you pass a Bimap pointer, the image you read will always be returned as 24 bits RGB, that's because the function assumes you will use the various LabWindows bitmap handling functions which accept only 24 bits RGB.
If you don't pass Bitmap, the image read will have its alpha channel stripped and will be either RGB or greyscale.     v    Empty array of bytes, passed by reference, containing the pixel information.
You must free(Bits) after you are done.     a    Return the number of bytes on each scan line of the image.
Should be equal to Width*PixDepth/8
     k    Number of bits per pixel (should be 24 in most cases or 8 in case of Greyscale image with Bitmap==NULL).
          Width of the image, in pixels.     !    Height of the image, in pixels.    � �          FileName                          �p���       Error                              $            Bitmap                            w $ �         Bits                              � $ �          RowBytes                          ^ $r          PixDepth                          � j           Width                             � j �          Height                             ""    	            	            	            	            	            	            	            R    Read an image and fill a control with it. Only some kinds of controls are valid.     %    Panel where is located the control.     �    Control to read from the file.
It must be one of: 
    - picture controls
    - picture rings
    - picture buttons
    - graph controls     =    Name of the PNG file to read. Should have extension ".png".     #    Return TRUE if an error happened.     �    For picture rings, this argument is the zero-based index of a particular image in the ring.

For graphs, this argument is the plotHandle returned from PlotBitmap.

For picture controls and picture buttons, this argument is ignored.    ~ 8            Panel                             � 8 �          Control                           = y          FileName                          � �����       Error                             � 8          ImageID                            0    0    ""    	            0       Read a file's properties without loading the bitmap:
- size (width and height)
- number of color channels (1 or 3)
- bytes per row
- bits per pixel

NOTE: You can call Png_GetParameters or Png_GetFileText... after this command to get additional information.     >    Name of the JPEG file to save. Should have extension ".jpg".     #    Return TRUE if an error happened.     `    Return the number of bytes on each scan line of the image.
Should be equal to 8*Width/PixDepth     B    Number of bits per pixel will return 8 (greyscale) or 24 (RGB).
          Width of the image, in pixels.     !    Height of the image, in pixels.     B    Number of bits per pixel will return 8 (greyscale) or 24 (RGB).
    � �          FileName                          p���       Error                             D %            RowBytes                          � % �          PixDepth                          � j           Width                              j �          Height                            G % �          NbChan                             ""    	            	            	            	            	            	            p    Get some parameters of the last PNG file that was read.
Calling this function is optional.
You can pass NULL.
     z    The Gamma of the image.
Default = 0.45455 on most PC monitors.
You can pass NULL if you don't want to recover the value.     ^    If the image is interlaced or not.
You can pass NULL if you don't want to recover the value.     �    Resolution of the image, in pixels per cm.
Note: 2.54 dpi = 1 pixel/cm.
You can pass NULL if you don't want to recover the value.     �    Compression level, from 0 (no compression) to 9 (best compression).
Default is -1 (=6) and fastest is 1.
You can pass NULL if you don't want to recover the value.     �    Return TRUE if an Alpha layer is present in the image and the image is 3*8 bits.
You can pass NULL if you don't want to recover the value.    � $           Gamma                             L $ � �       Interlaced                        � $         Resolution (ppcm)                 = �    �      CompressionLevel                  � � �          Alpha32                                   0.45455    	            	           	            	            O    Returns the title of the last PNG image file read.
May be an empty string "".     Z    Pointer to the title string of the last file read, or to an empty string if not present.    "  t n��   �    Title                              	            P    Returns the Author of the last PNG image file read.
May be an empty string "".     [    Pointer to the Author string of the last file read, or to an empty string if not present.    # t n��   �    Author                             	            U    Returns the Description of the last PNG image file read.
May be an empty string "".     `    Pointer to the Description string of the last file read, or to an empty string if not present.    $ t n��   �    Description                        	            S    Returns the Copyright of the last PNG image file read.
May be an empty string "".     ^    Pointer to the Copyright string of the last file read, or to an empty string if not present.    %  t n��   �    Copyright                          	            W    Returns the Creation Time of the last PNG image file read.
May be an empty string "".     a    Pointer to the CreationTime string of the last file read, or to an empty string if not present.    && t n��   �    CreationTime                       	            `    Returns the Software that created the the last PNG image file read.
May be an empty string "".     ]    Pointer to the Software string of the last file read, or to an empty string if not present.    '8 t n��   �    Software                           	            T    Returns the Disclaimer of the last PNG image file read.
May be an empty string "".     _    Pointer to the Disclaimer string of the last file read, or to an empty string if not present.    (: t n��   �    Disclaimer                         	            Q    Returns the Warning of the last PNG image file read.
May be an empty string "".     \    Pointer to the Warning string of the last file read, or to an empty string if not present.    ); t n��   �    Warning                            	            P    Returns the Source of the last PNG image file read.
May be an empty string "".     [    Pointer to the Source string of the last file read, or to an empty string if not present.    *8 t n��   �    Source                             	            Q    Returns the Comment of the last PNG image file read.
May be an empty string "".     \    Pointer to the Comment string of the last file read, or to an empty string if not present.    +5 t n��   �    Comment                            	            >    Saves a bitmap already in memory into a compressed PNG file.    t    The array which is filled with the bits that determine the colors to be displayed on each pixel of the image.

This array is returned by the GetBitmapData() function.

The number of bits in the image for each pixel is equal to
the Pixel Depth.  The currently supported pixel depth is 8, the bits are the RGB values.  The color thereby determined is displayed at the location of that pixel.  

The first pixel in the array is at the top, left corner of the image.  The pixels in the array are in row-major order.

If you are using a 1-bit PixelDepth (black&white image), each byte represents 8 pixels (0=black, 1=white). In that case, RowBytes=Width/8.

If you are using a 8-bit PixelDepth (greyscale image), each pixel is represented by a byte (0=black, 0xFF=white). In that case, RowBytes=Width.

If you are using a 24-bit Pixel Depth, each pixel is represented by a 24-bit RGB value of the form 0xRRGGBB, where RR,GG, and BB represent the red, green and blue intensity of the color.  The RR byte should always be at the lowest memory address of the three bytes.

If you are using a 32-bit Pixel Depth, each pixel is represented by a 32-bit RGB value of the form 0x00RRGGBB, where RR,GG, and BB represent the red, green and blue intensity of the color.  The 32-bit value is treated as a native 32-bit integer value for the platform.  The most significant byte is always ignored.  The BB byte should always be in the least significant byte.  On a little-endian platform (for example, Intel processors), BB would be at the lowest memory address.  
On a big-endian platform (for example, Motorola processors), BB would be at the highest address.  Note that this is different from the 24-bit Pixel Depth.


NOTE: if you set are in 32 bit mode and Alpha32 is set to TRUE, the image is saved as 3*8 RGB + alpha. The alpha layer contains the current selection, and many programs (IE5) cannot display it correctly (or it's CVI that got things wrong).
If you set Alpha32 to FALSE, the Bits are compressed to 24 bits: they are changed and you should NOT use the BitmapID further on in the program (except to release it). This also applies to greyscale with Alpha channel.     �    Number of bytes on each scan line of the image.
Should be equal to 
 - 3*Width/PixDepth in 24 bit mode 
 - 4*Width/PixDepth in 32 bit mode.
 - Width in 8 bit mode (greyscale)
 - Width/8 in B/W mode

It is returned by the GetBitmapData() function.     �    Number of bits per pixel . Should be:
- 32 in 32-bit mode
- 24 in 24 bit mode
- 8 for greyscale images
- 1 for B&W mode (8 pixels per byte)

It is returned by the GetBitmapData() function.     Q    Width of the image, in pixels.

It is returned by the GetBitmapData() function.     R    Height of the image, in pixels.

It is returned by the GetBitmapData() function.     =    Name of the PNG file to save. Should have extension ".png".     #    Return TRUE if an error happened.    ,  $ (          Bits                              4� $ �          RowBytes                          5� $          PixDepth                          6b _ (          Width                             6� _ �          Height                            7 � (    �    FileName                          7Z �z���       Error                              0    0    0    0    0    ""    	            8    Saves a panel or a control into a compressed PNG file.     @    Panel to save (if Control=0), or panel of the control to save.     :    Control to save (0 if you want to save the whole panel).     =    Name of the PNG file to save. Should have extension ".png".     #    Return TRUE if an error happened.    9c $            Panel                             9� $ �          Control                           9� V          FileName                          :2 �����       Error                              0    0    ""    	            �    Set some parameters for all PNG files that will be saved afterwards.
Calling this function is optional.
Defaults are:
  - Gamma=0.45455
  - No Interlacing
  - resolution at 72 Dpi
  - Compression set to Default (=6)     :    The Gamma of the image.
Default = 1 on most PC monitors.     ?    If the image is interlaced or not.
Default is not interlaced.     \    Resolution of the image, in pixels per cm.
Default is 28ppcm.
Note: 2.54 dpi = 1 pixel/cm.     �    Compression level, from 0 (no compression) to 9 (best compression).
Default is -1 (=6) and fastest is 1.
Between 6 and 9 the gain is typically 10% size on large photos.
Don't use 0 unless for tests.    �    If your graphic card has 32-bit mode, the current selection gets saved into the alpha channel. 
If you set this option to TRUE, the file is saved with the alpha channel, which is a problem with many graphic programs. If you set it to FALSE, the image get compressed to 24 bit RGB, no alpha. The Bits passed to SaveBitmapAsPng are changed and you should NOT use the BitmapID further on in the program (except to release it).

This has no influence in 16/24-bit RGB mode.

Default is FALSE.    <. .           Gamma                             <p $ � �       Interlaced                        <� . �         Resolution (ppcm)                 = �   �      CompressionLevel                  =� ~ <          Alpha32                            0.45455    Yes 1 No 0    28               }Default Z_DEFAULT_COMPRESSION None Z_NO_COMPRESSION Fastest Z_BEST_SPEED 2 2 3 3 4 4 5 5 6 6 7 7 8 8 Best Z_BEST_COMPRESSION    On 1 Off 0    �    Set the (short) title of the PNG file.

Optional function.
If you do not call this function, then no text information is written to the PNG file.

This text is retain for all further files saved. To remove the text, pass NULL to the function.     s    String containing the title/comment/etc for the next images.
If you set it to NULL, then no information is saved.     h    To compress the comment or not.
Until the text gets around 1000 bytes, it is not worth compressing it.    B� 3     ^    Str                               C e Z   �      Compressed                         ""    Zipped TRUE No FALSE    �    Set the author text of the PNG file.

Optional function.
If you do not call this function, then no text information is written to the PNG file.

This text is retain for all further files saved. To remove the text, pass NULL to the function.     s    String containing the title/comment/etc for the next images.
If you set it to NULL, then no information is saved.     h    To compress the comment or not.
Until the text gets around 1000 bytes, it is not worth compressing it.    E 3     ^    Str                               E� e Z   �      Compressed                         ""    Zipped TRUE No FALSE   /    Set the (possibly long) description text of the PNG file.
If long, the text should be compressed.

Optional function.
If you do not call this function, then no text information is written to the PNG file.

This text is retain for all further files saved. To remove the text, pass NULL to the function.     s    String containing the title/comment/etc for the next images.
If you set it to NULL, then no information is saved.     h    To compress the comment or not.
Until the text gets around 1000 bytes, it is not worth compressing it.    G� 3     ^    Str                               H- e Z   �      Compressed                         ""    Zipped TRUE No FALSE    �    Set the copyright text of the PNG file.

Optional function.
If you do not call this function, then no text information is written to the PNG file.

This text is retain for all further files saved. To remove the text, pass NULL to the function.     s    String containing the title/comment/etc for the next images.
If you set it to NULL, then no information is saved.     h    To compress the comment or not.
Until the text gets around 1000 bytes, it is not worth compressing it.    J" 3     ^    Str                               J� e Z   �      Compressed                         ""    Zipped TRUE No FALSE       Set the creation date information text of the PNG file.

Optional function.
If you do not call this function, then no text information is written to the PNG file.

This text is retain for all further files saved. To remove the text, pass NULL to the function.     s    String containing the title/comment/etc for the next images.
If you set it to NULL, then no information is saved.     h    To compress the comment or not.
Until the text gets around 1000 bytes, it is not worth compressing it.    L� 3     ^    Str                               M e Z   �      Compressed                         ""    Zipped TRUE No FALSE       Set the name of the software that created the PNG file.

Optional function.
If you do not call this function, then no text information is written to the PNG file.

This text is retain for all further files saved. To remove the text, pass NULL to the function.     s    String containing the title/comment/etc for the next images.
If you set it to NULL, then no information is saved.     h    To compress the comment or not.
Until the text gets around 1000 bytes, it is not worth compressing it.    O" 3     ^    Str                               O� e Z   �      Compressed                         ""    Zipped TRUE No FALSE   .    Set the (possibly long) disclaimer text of the PNG file.
If long, the text should be compressed.

Optional function.
If you do not call this function, then no text information is written to the PNG file.

This text is retain for all further files saved. To remove the text, pass NULL to the function.     s    String containing the title/comment/etc for the next images.
If you set it to NULL, then no information is saved.     h    To compress the comment or not.
Until the text gets around 1000 bytes, it is not worth compressing it.    Q� 3     ^    Str                               RF e Z   �      Compressed                         ""    Zipped TRUE No FALSE   F    Set the (possibly long) warning text of the PNG file (for explicit pictures...).
If long, the text should be compressed.

Optional function.
If you do not call this function, then no text information is written to the PNG file.

This text is retain for all further files saved. To remove the text, pass NULL to the function.     s    String containing the title/comment/etc for the next images.
If you set it to NULL, then no information is saved.     h    To compress the comment or not.
Until the text gets around 1000 bytes, it is not worth compressing it.    T� 3     ^    Str                               U e Z   �      Compressed                         ""    Zipped TRUE No FALSE   )    Set the text explaining the source of the PNG file.
If long, the text should be compressed.

Optional function.
If you do not call this function, then no text information is written to the PNG file.

This text is retain for all further files saved. To remove the text, pass NULL to the function.     s    String containing the title/comment/etc for the next images.
If you set it to NULL, then no information is saved.     h    To compress the comment or not.
Until the text gets around 1000 bytes, it is not worth compressing it.    W0 3     ^    Str                               W� e Z   �      Compressed                         ""    Zipped TRUE No FALSE   )    Set a (possibly long) comment text of the PNG file.
If long, the text should be compressed.

Optional function.
If you do not call this function, then no text information is written to the PNG file.

This text is retain for all further files saved. To remove the text, pass NULL to the function.     s    String containing the title/comment/etc for the next images.
If you set it to NULL, then no information is saved.     h    To compress the comment or not.
Until the text gets around 1000 bytes, it is not worth compressing it.    Y� 3     ^    Str                               ZO e Z   �      Compressed                         ""    Zipped TRUE No FALSE    �    Swap the bits for big endian machines (Mac or Unix, NOT the PC).
Must be called before saving the file this should be applied only to 32 bit images.     :    Array of 32 bits color values (RGBA) to byte-swap (ABGR)         Array size    [� $           Colors                            \' $ �          Nb                                 	            0   R    Transform a RGB image to a greyscale one (one pixel per byte).

Ratio is one byte out of 3 in 24-bit RGB mode, and one out of 4 in 32-bit mode. So we divide the image size by 3 or 4.

NOTE: we use the Green channel as reference, if red or blue are different, then screw it !
The transformation can be done in place, that is Dest=Source
     %    Destination array of NbPixels bytes     &    Source array of NbPixels*Ratio bytes     2    Number of bytes to copy.  (=Width*Height pixels)     c    One byte out of 3 in 24 bit mode,
one byte out of 4 or 32 bit mode.

(=PixDepth/8=RowBytes/Width)    ^ $           Dest                              ^= $ �         Source                            ^k ~           NbPixels                          ^� ~ �          Ratio                              	            0    0    0   h    Transform a RGB image to a Black and white one (8 pixels per byte), 
Ratio is one bit out of 3 bytes in 24-bit RGB mode, 
and one bit out of 4 bytes in 32-bit mode
So we divide the image size by 24 or 32.

NOTE: we use the Green channel as reference, if red or blue are different, then screw it !
The transformation can be done in place, that is Dest=Source
     /    Destination array of (Width+7)/8*Height bytes     *    Source array of Width*Height*Ratio bytes     7    one out of 3 or 4 in 24 or 32 bit mode.
(=PixDepth/8)     =    Width of the image.
Width*Height = number of bytes to copy.     >    Height of the image.
Width*Height = number of bytes to copy.     �    Color level above which to set to white.

if =1,   then everything different from black is set to white,
if =255, then everything different from white is set to black.    ao $           Dest                              a� $ �         Source                            a� ~           Ratio                             b ~ �          Width                             b\ ~J          Height                            b� �           ColorThreshold                     	            0    0    0    0    0 ����         c  "             K.        ReadBitmapFromFile                                                                                                                      ����         $  �             K.        ReadControlFromFile                                                                                                                     ����         �  �             K.        GetProperties                                                                                                                           ����         R   }             K.        GetParameters                                                                                                                           ����         !�  "�             K.        GetTextTitle                                                                                                                            ����         "�  #~             K.        GetTextAuthor                                                                                                                           ����         #�  $�             K.        GetTextDescription                                                                                                                      ����         $�  %�             K.        GetTextCopyright                                                                                                                        ����         %�  &�             K.        GetTextCreationTime                                                                                                                     ����         &�  '�             K.        GetTextSoftware                                                                                                                         ����         '�  (�             K.        GetTextDisclaimer                                                                                                                       ����         (�  )�             K.        GetTextWarning                                                                                                                          ����         )�  *�             K.        GetTextSource                                                                                                                           ����         *�  +�             K.        GetTextComment                                                                                                                          ����         +�  7�             K.        SaveBitmapToFile                                                                                                                        ����         9#  :]             K.        SavePanelControlToFile                                                                                                                  ����         ;M  ?�             K.        SetParameters                                                                                                                           ����         A�  C�             K.        SetTextTitle                                                                                                                            ����         D  E�             K.        SetTextAuthor                                                                                                                           ����         F{  H�             K.        SetTextDescription                                                                                                                      ����         I%  K             K.        SetTextCopyright                                                                                                                        ����         K�  M�             K.        SetTextCreationTime                                                                                                                     ����         N  P             K.        SetTextSoftware                                                                                                                         ����         P�  R�             K.        SetTextDisclaimer                                                                                                                       ����         S>  Uw             K.        SetTextWarning                                                                                                                          ����         U�  X             K.        SetTextSource                                                                                                                           ����         X�  Z�             K.        SetTextComment                                                                                                                          ����         [G  \;             K.        SwapEndian                                                                                                                              ����         \�  _             K.        CopyRGBtoGray                                                                                                                           ����         _�  cS             K.        CopyRGBtoBW                                                                                                                                                                                                                       
DReadFromPng                                                                          �Read Bitmap From PNG File                                                            �Read Control From PNG File                                                           �Get image Properties                                                                 �Get PNG parameters                                                                   �Get Text Title                                                                       �Get Text Author                                                                      �Get Text Description                                                                 �Get Text Copyright                                                                   �Get Text Creation Time                                                               �Get Text Software                                                                    �Get Text Disclaimer                                                                  �Get Text Warning                                                                     �Get Text Source                                                                      �Get Text Comment                                                                    
�SaveAsPng                                                                            �Save Bitmap as PNG                                                                   �Save panel or control as PNG                                                         �Set PNG parameters                                                                   �Set Text Title                                                                       �Set Text Author                                                                      �Set Text Description                                                                 �Set Text Copyright                                                                   �Set Text Creation Time                                                               �Set Text Software                                                                    �Set Text Disclaimer                                                                  �Set Text Warning                                                                     �Set Text Source                                                                      �Set Text Comment                                                                    6Bitmap Manip                                                                         �Swap the bits of an image                                                            �RBG to Greyscale image conversion                                                    �RGB to Black&White image conversion                                             