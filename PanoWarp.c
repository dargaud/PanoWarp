/////////////////////////////////////////////////////////////////////////////////////
// PROGRAM:   PanoWarp.exe                                                         //
// COPYRIGHT: Guillaume Dargaud 1998-2001 - Freeware                               //
// PURPOSE:   Deformation of bitmap pictures in order to stitch them in a panorama //
// INSTALL:   run SETUP. It will also install the LabWindows/CVI runtime engine.   //
// HELP:      available by right-clicking an item.                                 //
// TUTORIAL:  http://www.gdargaud.net/Hack/PanoWarp.html                           //
/////////////////////////////////////////////////////////////////////////////////////

#define _VER_ "2.5"			// Version 2.5
#define _WEB_ "http://www.gdargaud.net/Hack/PanoWarp.html"
#define _COPY_ "� 1998-2002 Guillaume Dargaud"

#include <userint.h>
#include <utility.h>
#include <ansi_c.h>
#include <cvirte.h>    /* Needed if linking in external compiler; harmless otherwise */
// #include <formatio.h>
#include "wintools.h"
#include "Def.h"

#include "ReadSavePng.h"
#include "ReadSaveJpeg.h"
#include "ImgConv.h"
#include "PanoWarp.h"


////////////////////////// Globals ////////////////////////////////////////////
static int Pnl=0, DecimationIn=1, DecimationOut=1;
static char FileName[MAX_PATHNAME_LEN]="";
static char Err[255];

static int BmpIn=0, BmpInD=0, BmpOut=0, BmpOutD=0, Status, PixDepth=24;
static int HeightIn, WidthIn, RowByteIn;
static int HeightInD, WidthInD, RowByteInD, MinWidth;
static unsigned char *Mask=NULL, *BitsIn=NULL, *BitsInD=NULL, *NewMask=NULL, *BitsOut=NULL, *BitsOutD=NULL;
static int *ColorTable;

static int Focal, Pcd;
static double Psi, cPsi, sPsi, tPsi, Resol, H, LenX, LenY;
static double AlphaMax, PhiMin, PhiMax;

static long DefaultColor=0, DCr, DCg, DCb;
static DoLine=FALSE;
static const double LineAngle=10*Pi/180;

static int Deform=FALSE;
static double Factor=500;
static int WidthOut, HeightOut, WidthOutD, HeightOutD, RowByteOutD;

static int VSize[13]={0, 
					64, 128, 256, 512, 1024, 2048, 4096, 	// PCD formats
					480, 600, 768, 1024, 1200 };			// PC screen formats

///////////////////////// Pre-declarations ////////////////////////////////////

static void SizeHoriz(void);
static void SetFactor(double F);
static void SetDeform(int D);
static void SetDefaultColor(long D);
static int ComputeInDecimation(void);
static int ComputeOutDecimation(void);
static void TrueSize(void);
static void ReadParam(void);
static void WriteParam(void);


///////////////////////// Main ////////////////////////////////////////////////

int main (int argc, char *argv[]) {
	if (InitCVIRTE (0, argv, 0) == 0)    /* Needed if linking in external compiler; harmless otherwise */
		return -1;    /* out of memory */
	if ((Pnl = LoadPanel (0, "PanoWarp.uir", PNL)) < 0) {
		MessagePopup("Error!", "Could not find file PanoWarp.uir in program directory.\nAborting.");
		return -1;
	}
	DisplayPanel (Pnl);
	ReadParam();
	SetDeform(0);
	SizeHoriz();
	EnableDragAndDrop (Pnl);

	RunUserInterface ();		// Program starts here
	
	WriteParam();
	return 0;
}


static void ReadParam(void) {
	long D;
	int Size, St;
	St=WT_RegReadBinary (WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\PanoWarp", "Focal", (char*)&Focal, sizeof(Focal), &Size);
	if (St!=0) {								// No key, first run or protected
//		WT_GetErrorString (St, Descr);
		GetCtrlVal (Pnl, PNL_FOCAL, &Focal);
		cb_Pcd (0,0,EVENT_COMMIT,NULL,0,0);
		SetDefaultColor(0x0);
		return;
	}
	St=WT_RegReadBinary (WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\PanoWarp", "Pcd", (char*)&Pcd, sizeof(Pcd), &Size);
	if (Pcd==0)
	St=WT_RegReadBinary (WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\PanoWarp", "Resol", (char*)&Resol, sizeof(Resol), &Size);

	St=WT_RegReadBinary (WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\PanoWarp", "DefaultColor", (char*)&D, sizeof(D), &Size);
	SetDefaultColor(D);

	SetCtrlVal (Pnl, PNL_FOCAL, Focal);
	SetCtrlVal (Pnl, PNL_FOCAL, Focal);
	cb_Pcd (0,0,EVENT_COMMIT,NULL,0,0);

	SetCtrlVal(Pnl, PNL_HELP, "[Right][Click] an item\nfor Help or go to\n" _WEB_);
	SetCtrlVal(Pnl, PNL_COPYRIGHT, "PanoWarp v" _VER_ "\n" _COPY_);
}


static void WriteParam(void) {
	WT_RegWriteBinary(WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\PanoWarp", "Focal", (char*)&Focal, sizeof(Focal));
	WT_RegWriteBinary(WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\PanoWarp", "Pcd", (char*)&Pcd, sizeof(Pcd));
	WT_RegWriteBinary(WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\PanoWarp", "Resol", (char*)&Resol, sizeof(Resol));
	WT_RegWriteBinary(WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\PanoWarp", "DefaultColor", (char*)&DefaultColor, sizeof(DefaultColor));
}


///////////////////////// UpdateParam /////////////////////////////////////////
static void UpdateParam(void) {
	double AlphaLT, AlphaLC, AlphaLB, AlphaRT, AlphaRC, AlphaRB, 
		   PhiLT, PhiCT, PhiRT, PhiLB, PhiCB, PhiRB;
	double X,Y;
	cPsi=cos(Psi);
	sPsi=sin(Psi);
	tPsi=tan(Psi);
	SetCtrlVal(Pnl, PNL_LENX, LenX=WidthIn/Resol);
	SetCtrlVal(Pnl, PNL_LENY, LenY=HeightIn/Resol);
	SetCtrlAttribute (Pnl, PNL_HORIZ, ATTR_MAX_VALUE, LenY/2);
	SetCtrlAttribute (Pnl, PNL_HORIZ, ATTR_MIN_VALUE, -LenY/2);

#define Alpha(x,y) (X=(x)/Focal,Y=(y)/Focal,                    \
					atan(X/(cPsi-Y*sPsi)))
#define Phi(x,y) (X=(x)/Focal,Y=(y)/Focal,                      \
					atan( (Y+tPsi)/sqrt(                        \
						X*X/(cPsi*cPsi) + (1-Y*tPsi)*(1-Y*tPsi) \
					) ))
	AlphaLT=Alpha(-LenX/2,  LenY/2);
	AlphaLC=Alpha(-LenX/2,       0);
	AlphaLB=Alpha(-LenX/2, -LenY/2);
	AlphaRT=Alpha( LenX/2,  LenY/2);
	AlphaRC=Alpha( LenX/2,       0);
	AlphaRB=Alpha( LenX/2, -LenY/2);
	AlphaMax=MAX3(AlphaRT, AlphaRC, AlphaRB);	// - to +

	PhiLT  =  Phi(-LenX/2,  LenY/2);
	PhiCT  =  Phi(      0,  LenY/2);
	PhiRT  =  Phi( LenX/2,  LenY/2);
	PhiLB  =  Phi(-LenX/2, -LenY/2);
	PhiCB  =  Phi(      0, -LenY/2);
	PhiRB  =  Phi( LenX/2, -LenY/2);
	PhiMin=MIN3(PhiLB, PhiCB, PhiRB);
	PhiMax=MAX3(PhiLT, PhiCT, PhiRT);
	
	SetCtrlVal(Pnl, PNL_ALPHAMAX, RadToDeg*AlphaMax);
	SetCtrlVal(Pnl, PNL_PHIMAX, RadToDeg*PhiMax);
	SetCtrlVal(Pnl, PNL_PHIMIN, RadToDeg*PhiMin);
	SetFactor(WidthIn/(2*AlphaMax));	// AutoSet?
#undef Alpha
#undef Phi
}

int CVICALLBACK cb_Quit (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			if (ColorTable!=NULL) free(ColorTable); ColorTable=NULL;
			if (BitsIn!=NULL) free(BitsIn); BitsIn=NULL;
			if (Mask!=NULL) free(Mask); Mask=NULL;
			QuitUserInterface (0);
			break;
		case EVENT_RIGHT_CLICK:
   			MessagePopup("Quit", "Hey, seriously, what did you expect to read here ?!?");
			break;
	}
	return 0;
}


////////////////////////// GetBitmap //////////////////////////////////////////

#define ORIG_PASTE 0
#define ORIG_BMP_FILE 1
#define ORIG_PNG_FILE 2
#define ORIG_JPEG_FILE 3

static void GetBitmap(const int Origin, const char* FileName) {
	int Available, Error;

	SetWaitCursor(1);
	if (ColorTable!=NULL) free(ColorTable); ColorTable=NULL;
	if (BitsIn!=NULL) free(BitsIn); BitsIn=NULL;
	if (Mask!=NULL) free(Mask); Mask=NULL;
	if (BitsIn!=NULL) free(BitsIn); BitsIn=NULL;
	if (BitsInD!=NULL) free(BitsInD); BitsInD=NULL;

//	if (BmpIn!=0) DiscardBitmap(BmpIn); BmpIn=0;

	SetDeform(FALSE);
	switch (Origin) {
		case ORIG_PASTE:		// Get Bitmap from paste operation
			SetPanelAttribute (Pnl, ATTR_TITLE, "PanoWarp - Picture warping for panorama stitching");
			Error=ClipboardGetBitmap (&BmpIn, &Available);
			if (Error!=0) {
				SetWaitCursor (0);
				MessagePopup ("Error !", GetUILErrorString(Error));
				return ;
			} else if (!Available) {
				SetWaitCursor (0);
				MessagePopup ("Warning !", "The clipboard does not seem to contain a valid image.");
				return ;
			}
			Status=0;
			goto Jump;

		case ORIG_BMP_FILE:		// Get Bitmap from BMP file
			Status = GetBitmapFromFile (FileName, &BmpIn);

			Jump:
			if (Status!=0) { MessagePopup("Error!",GetUILErrorString (Status)); /*QuitUserInterface(1);*/ }
			Status = AllocBitmapData (BmpIn, &ColorTable, &BitsIn, &Mask);
			if (Status!=0) { MessagePopup("Error!",GetUILErrorString (Status)); /*QuitUserInterface(1);*/ }
			if (ColorTable!=NULL) {	
				MessagePopup ("Warning !", "This program operates only on\n16 million color files.\nExiting.");
				QuitUserInterface(1); 
			}
			if (Mask!=NULL) {	
				MessagePopup ("Warning !", "This program operates only on\nnon masked files.\nExiting.");
				QuitUserInterface(1); 
			}
	
			Status = GetBitmapData (BmpIn, &RowByteIn, &PixDepth, &WidthIn, &HeightIn, ColorTable, BitsIn, Mask);
/* ??? */	if (BmpIn!=0) DiscardBitmap(BmpIn); BmpIn=0;
			break;
		
		case ORIG_PNG_FILE:		// Get Bitmap from PNG file
			Status = Png_ReadBitmapFromFile (/* &BmpIn*/ NULL, &BitsIn, &RowByteIn, &PixDepth, &WidthIn, &HeightIn, FileName);
			if (Status) Status=-12;
			Png_GetParameters (NULL, NULL, &Resol, NULL, NULL);
			SetCtrlVal(Pnl, PNL_RESOLUTION, Resol/=10);
			SetCtrlVal(Pnl, PNL_DPI, Resol*25.4);
			break;

		case ORIG_JPEG_FILE:		// Get Bitmap from JPEG file
			Status = Jpg_ReadBitmapFromFile (/* &BmpIn*/ NULL, &BitsIn, &RowByteIn, &PixDepth, &WidthIn, &HeightIn, FileName);
			if (Status) Status=-12;
//			Png_GetParameters (NULL, NULL, &Resol, NULL, NULL);
//			SetCtrlVal(Pnl, PNL_RESOLUTION, Resol/=10);
//			SetCtrlVal(Pnl, PNL_DPI, Resol*25.4);
			break;
	}

	if (Status!=0) { MessagePopup("Error!",GetUILErrorString (Status)); /*QuitUserInterface(1);*/ }

	if (PixDepth==32) {
		Jpg_CopyRGB32to24 (BitsIn, BitsIn, WidthIn*HeightIn);
		RowByteIn=3*RowByteIn/4;
		PixDepth=24;
	}
	ComputeInDecimation();
	
	SetCtrlVal(Pnl, PNL_WIDTH, WidthIn);
	SetCtrlVal(Pnl, PNL_HEIGHT, HeightIn);
	UpdateParam();

//	if (BmpInD!=0) DiscardBitmap(BmpInD); BmpInD=0;
	if (PixDepth==8) { 
		int i; 
		if (ColorTable!=NULL) free(ColorTable);
		ColorTable=Calloc(256, int); 
		for (i=0; i<256; i++) ColorTable[i]=i<<16 | i<<8 | i; 
	}
	Status = NewBitmap (RowByteInD/*-1*/, PixDepth, WidthInD, HeightInD, ColorTable, BitsInD, NULL, &BmpInD);
	if (Status!=0) { MessagePopup("Error!",GetUILErrorString (Status)); /*QuitUserInterface(1);*/ }
	
	Status = SetCtrlBitmap (Pnl, PNL_PIC, 0, BmpInD);
	if (Status!=0) { MessagePopup("Error!",GetUILErrorString (Status)); /*QuitUserInterface(1);*/ }
	TrueSize();
}


static char Drive[MAX_DRIVENAME_LEN], Dir[MAX_DIRNAME_LEN];

// Return TRUE if file not readable
static BOOL LoadFile(void) {
	int Status;
	SetWaitCursor(1);
	
	if (BmpIn!=0) DiscardBitmap (BmpIn); BmpIn=0;

	if (toupper(FileName[strlen(FileName)-3])=='P' and
		toupper(FileName[strlen(FileName)-2])=='N' and
		toupper(FileName[strlen(FileName)-1])=='G') 
		GetBitmap(ORIG_PNG_FILE, FileName);
	else
	if (toupper(FileName[strlen(FileName)-3])=='J' and
		toupper(FileName[strlen(FileName)-2])=='P' and
		toupper(FileName[strlen(FileName)-1])=='G' or
		toupper(FileName[strlen(FileName)-4])=='J' and
		toupper(FileName[strlen(FileName)-3])=='P' and
		toupper(FileName[strlen(FileName)-2])=='E' and
		toupper(FileName[strlen(FileName)-1])=='G') 
		GetBitmap(ORIG_JPEG_FILE, FileName);
	else {
		MessagePopup("Error", "Selected file is not readable");
		return TRUE;
	}
	
	SetWaitCursor(0);
//			else SetCanvasSize();
	return FALSE;
}

int CVICALLBACK cb_Load (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	char Title[MAX_PATHNAME_LEN+100];
	int Status;
	
	switch (event) {
		case EVENT_COMMIT:
			Status = FileSelectPopup ("", "*.png;*.jpg;*.jpeg", "*.png;*.jpg;*.jpeg",
									  "Open png/jpg file", VAL_LOAD_BUTTON,
									  0, 0, 1, 1, FileName);
			if (Status!=VAL_EXISTING_FILE_SELECTED) return 0;
			LoadFile();			
			break;
		case EVENT_RIGHT_CLICK:
   			MessagePopup("Load", "Selects and loads a PNG or JPG image file.");
			return 1;
	}
	return 0;
}


int CVICALLBACK cb_Save (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	char Title[MAX_PATHNAME_LEN+100], Response[5]="75";
	static BOOL FirstCallPng=TRUE, FirstCallJpg=TRUE, LastPng=TRUE;
	static int CompressionJpeg=75;
	time_t Time=time(NULL);
	char *Dot, Sample[MAX_PATHNAME_LEN];

	switch (event) {
		case EVENT_COMMIT:
			Dot=strrchr (FileName, '.');
			strncpy(Sample, FileName, Dot-FileName);
			Sample[Dot-FileName]='\0';
			strcat(Sample, "W.png");
			

			Status = FileSelectPopup (Dir, Sample /*LastPng?"*.png":"*.jpg"*/, "*.png;*.jpg;*.jpeg",
									  "Save warped image to ?", VAL_SAVE_BUTTON,
									  0, 0, 1, 1, FileName);
			if (Status!=VAL_EXISTING_FILE_SELECTED and Status!=VAL_NEW_FILE_SELECTED) return 0;
			SetWaitCursor(1);
			
/*			// Save to raw file for tests.	
			FileName[strlen(FileName)-3]='r';
			FileName[strlen(FileName)-2]='a';
			FileName[strlen(FileName)-1]='w';
			ArrayToFile (FileName, BitsOut, VAL_UNSIGNED_CHAR, PixDepth/8*WidthOut*HeightOut, 1,
						 VAL_GROUPS_TOGETHER, VAL_GROUPS_AS_COLUMNS,
						 VAL_CONST_WIDTH, 10, VAL_BINARY, VAL_TRUNCATE);
			FileName[strlen(FileName)-3]='p';
			FileName[strlen(FileName)-2]='n';
			FileName[strlen(FileName)-1]='g';
*/
			
			if (toupper(FileName[strlen(FileName)-3])=='P' and
				toupper(FileName[strlen(FileName)-2])=='N' and
				toupper(FileName[strlen(FileName)-1])=='G') {
				if (FirstCallPng) {
					Png_SetParameters (0.5, 0, Resol*10, Z_DEFAULT_COMPRESSION, 0);
					Png_SetTextTitle("Warped image", FALSE);
					Png_SetTextDescription("Imaged warped from original using ", FALSE);
					Png_SetTextCopyright("Image � ??? - Warping software " _COPY_, FALSE);
					Png_SetTextCreationTime(ctime(&Time), FALSE);
					Png_SetTextSoftware("PanoWarp.exe, freeware for MS Windows.", FALSE);
					Png_SetTextComment("This is an intermediate picture in the creation of a 360deg panorama", TRUE);
					FirstCallPng=FALSE;
				}
				Png_SaveBitmapToFile (BitsOut, PixDepth/8*WidthOut, PixDepth, WidthOut, HeightOut, FileName);
				LastPng=TRUE;
			}
			else
			if ( (toupper(FileName[strlen(FileName)-3])=='J' and
				toupper(FileName[strlen(FileName)-2])=='P' and
				toupper(FileName[strlen(FileName)-1])=='G') or 
				(toupper(FileName[strlen(FileName)-4])=='J' and
				toupper(FileName[strlen(FileName)-3])=='P' and
				toupper(FileName[strlen(FileName)-2])=='E' and
				toupper(FileName[strlen(FileName)-1])=='G') ) {
				if (FirstCallJpg) {
					PromptPopup ("JPEG compression quality ?",
								 "Please choose the compression ratio for the JPEG compression.\n"
								 "1=high compression, poor quality\n"
								 "100=no compression, highest quality\n"
								 "75=default compression, good quality",
								 Response, 4);
					CompressionJpeg=atoi(Response);
					if (strlen(Response)==0) CompressionJpeg=75;
					if (CompressionJpeg<1) CompressionJpeg=75;
					if (CompressionJpeg>100) CompressionJpeg=75;
					FirstCallJpg=FALSE;
				}
				Jpg_SaveBitmapToFile (BitsOut, PixDepth/8*WidthOut, PixDepth, WidthOut, HeightOut, FileName, CompressionJpeg);
				LastPng=FALSE;
			} 
			else MessagePopup("Error!", "Error, invalid file extension specified.");

			SetWaitCursor (0);
			break;
		case EVENT_RIGHT_CLICK:
   			MessagePopup("Load", "Save warped image to a compressed png or jpg file.");
			break;
	}
	return 0;
}


int CVICALLBACK cb_Pcd (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal (Pnl, PNL_PCD, &Pcd);
			if (Pcd==-1) {
				if (WidthIn>0 and HeightIn>0) Resol=sqrt(WidthIn*WidthIn+HeightIn*HeightIn)/sqrt(36*36+24*24);
				else Pcd==0;
			}
			if (Pcd>0 and Pcd<8) Resol=(1<<(Pcd+5))/24.;	// Kodak format
			else if (Pcd>7) Resol=VSize[Pcd]/24.;			// PC screen format
			else SetCtrlVal (Pnl, PNL_PCD, 0);
			
			SetCtrlVal(Pnl, PNL_RESOLUTION, Resol);
			SetCtrlVal(Pnl, PNL_DPI, Resol*25.4);
			UpdateParam();
			break;
		case EVENT_RIGHT_CLICK:
   			MessagePopup("PCD format", "Number of pixels of the original scan of the picture\n"
   										"(corresponds to Kodak PhotoCD or standard PC screen formats).\n"
   										"This is only used to simplify the setting of the px/mm resolution.\n"
   										"You can crop (but not resize) the picture before using PanoWarp.\n"
   										"If you do resize the pic, then you need to adjust the resolution\n"
   										"For instance, a 2700dpi scan resized to 50% becomes a 1350dpi scan.\n"
   										"\nIf you have a 24x36 camera and you scanned the entire image, you can use [Auto].");
			break;
	}
	return 0;
}

int CVICALLBACK cd_Resol (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
//		case EVENT_COMMIT:
		case EVENT_VAL_CHANGED:
			GetCtrlVal(Pnl, PNL_RESOLUTION, &Resol);
			SetCtrlVal(Pnl, PNL_DPI, Resol*25.4);
			SetCtrlVal (Pnl, PNL_PCD, 0);
			UpdateParam();
			break;
		case EVENT_RIGHT_CLICK:
   			MessagePopup("pc/mm", "Scanning resolution in px/mm,\n"
   									"expressed on the 24x36mm surface, not on a print.");
			break;
	}
	return 0;
}

int CVICALLBACK cb_Dpi (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
//		case EVENT_COMMIT:
		case EVENT_VAL_CHANGED:
			GetCtrlVal(Pnl, PNL_DPI, &Resol); Resol/=25.4;
			SetCtrlVal(Pnl, PNL_RESOLUTION, Resol);
			SetCtrlVal (Pnl, PNL_PCD, 0);
			UpdateParam();
			break;
		case EVENT_RIGHT_CLICK:
   			MessagePopup("pc/mm", "Scanning resolution in dpi,\n"
   									"expressed on the 24x36mm surface, not on a print.");
			break;
	}
	return 0;
}

int CVICALLBACK cb_Psi (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(Pnl, PNL_PSI, &Psi); Psi*=DegToRad;
			H=Focal*tan(Psi);
			SetCtrlVal(Pnl, PNL_HORIZON, H);
			SetCtrlVal(Pnl, PNL_HORIZ, H);
			UpdateParam();
			break;
		case EVENT_RIGHT_CLICK:
   			MessagePopup("Tilt angle", "Angle of the horizon in respect\nto the center of the picture..");
			break;
	}
	return 0;
}

int CVICALLBACK cb_Horizon (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
		case EVENT_VAL_CHANGED:
			GetCtrlVal(panel, control, &H);
			SetCtrlVal(Pnl, PNL_HORIZON, H);
			SetCtrlVal(Pnl, PNL_HORIZ, H);
			Psi=atan(H/Focal);
			SetCtrlVal(Pnl, PNL_PSI, Psi*RadToDeg);
			UpdateParam();
			break;
		case EVENT_RIGHT_CLICK:
   			MessagePopup("Horizon", "Elevation of the horizon above the center of the film.\n"
   									"Expressed in mm on the 24x36 film surface.\n"
   									"For easy use, set the vertical slider smack on the horizon.\n\n"
   									"Note: if the horizon is not a straight line after the deformation\n"
   									"      it means that one of the parameters is wrong (probably the resolution).");
			break;
	}
	return 0;
}

int CVICALLBACK cb_Focal (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
   			GetCtrlVal (Pnl, PNL_FOCAL, &Focal);
			UpdateParam();
			break;
		case EVENT_RIGHT_CLICK:
   			MessagePopup("Focal", "Focal of the lens used to take the pictures, for 24x36 camera film format.\n"
   									"If you don't know the lens type, try 35mm first.\n"
   									"\nDigital camera users:\n"
   									"The focal length and resolution are values you will find in the camera's specifications.\n"
   									"You have two choices: either use the real specifications or the 35mm equivalents:\n"
   									"Real Specifications:\n"
   									"  Your camera is for instance a 7mm 15x zoom with a 1/4\" CCD.\n"
   									"  It produces an image that is 600x800 pixels.\n"
   									"  Enter the focal as 7, then the resolution as 800x4=3200dpi.\n"
   									"  It should give you an image size of about 6.3mm.\n"
   									"\n35mm equivalent:\n"
   									"The camera manual also says that the equivalent focal of the lens is 35-500mm.\n"
   									"  Enter 35 as the focal.\n"
   									"  Choose auto or 600x800 as the image size.\n"
   									"  The displayed image size should be 24x36mm.");
			break;
	}
	return 0;
}

static void SetDeform(int D) {
	int i, j, PosX, PosY, pos, r, g, b, C, Progress;
	double Alpha, cAlpha, sAlpha, /*tAlpha,*/ AlphaInc;
	double Phi, /*cPhi, sPhi,*/ tPhi, PhiInc;
	double ctt,x,y;
	Deform=D;
	SetWaitCursor (1); // Turn on wait cursor
	
	SetCtrlVal(Pnl, PNL_DEFORM, Deform);
	SetCtrlAttribute (Pnl, PNL_HORIZ, ATTR_VISIBLE, !Deform);
	SetCtrlAttribute (Pnl, PNL_COPY, ATTR_DIMMED, !Deform);
	SetCtrlAttribute (Pnl, PNL_SAVE, ATTR_DIMMED, !Deform);
	GetCtrlAttribute (Pnl, Deform?PNL_CANVAS_4:PNL_CANVAS_2, ATTR_PICT_BGCOLOR, &C);
	SetCtrlAttribute (Pnl, PNL_PIC, ATTR_FRAME_COLOR, C);
	SetCtrlAttribute(Pnl, PNL_PIC, ATTR_DIMMED, Deform);
	if (!Deform) { 
		SetCtrlBitmap (Pnl, PNL_PIC, 0, BmpInD); 
		TrueSize();
		SetWaitCursor (0); // Turn off wait cursor
		return; 
	}

	AlphaInc=2*AlphaMax/WidthOut;
	PhiInc=(PhiMax-PhiMin)/HeightOut;
	if (BitsOut!=NULL) free(BitsOut); BitsOut=NULL;
	if (NewMask!=NULL) free(NewMask); NewMask=NULL;
	BitsOut=Calloc(PixDepth/8*HeightOut*WidthOut, unsigned char);
	if (BitsOut==NULL) { 
		SetWaitCursor (0); // Turn off wait cursor
		MessagePopup("Error!", "Out of memory, sorry..."); 
		return; 
	}
//			Status = NewBitmap (WidthOut/PixDepth*8, PixDepth, WidthOut, HeightOut, ColorTable,
//								BitsOut, NewMask, &BmpOut);
//			Status = AllocBitmapData (BmpIn, &ColorTable, &BitsIn, &Mask);
	Progress = CreateProgressDialog ("Warping picture...", "Percent Complete", 1, VAL_NO_INNER_MARKERS, "__Cancel");

	// For all horizontal angles
	for (i=0, Alpha=-AlphaMax; i<WidthOut; i++,Alpha+=AlphaInc) {
		if(i%10==0 and UpdateProgressDialog (Progress, 100*i/WidthOut, TRUE)) { 	// Cancel
			DiscardProgressDialog (Progress);
			SetDeform(FALSE); 
			SetWaitCursor (0); // Turn off wait cursor
			return; 
		}	
		//Alpha=AlphaMax*(2*i/(WidthOut-1)-1);
		cAlpha=cos(Alpha);
		sAlpha=sin(Alpha);
		
		// For all vertical angles
		for (j=0, Phi=PhiMin; j<HeightOut; j++,Phi+=PhiInc) {
			//Phi=PhiMin+(PhiMax-PhiMin)*i/(HeightOut-1);
			tPhi=tan(Phi);
			ctt=Focal/(cAlpha+tPhi*tPsi);
			x=sAlpha*ctt;
			y=(tPhi-cAlpha*tPsi)*ctt;
			PosX=WidthIn*(x/LenX+0.5);
			PosY=HeightIn*(y/LenY+0.5);

			if (PosX>=0 and PosX<WidthIn and PosY>=0 and PosY<HeightIn) {
				pos=PixDepth/8*(PosX+PosY*WidthIn);	// Red Byte position, source
				r=BitsIn[pos];
				if (PixDepth>8) { g=BitsIn[++pos]; b=BitsIn[++pos]; }
			} else { r=DCr; g=DCg; b=DCb; }
			
			pos=PixDepth/8*(i+j*WidthOut);				// Red byte position, dest
			BitsOut[pos]=r;
			if (PixDepth>8) { BitsOut[++pos]=g; BitsOut[++pos]=b; }
			
			// Debugging purpose
			if ( DoLine and 
				(fabs(fmod (Alpha, LineAngle))<AlphaInc or fabs(fmod (Phi, LineAngle))<PhiInc) ) {
				BitsOut[pos]=0x80;
				if (PixDepth>8) { BitsOut[pos+1]=BitsOut[pos+2]=0x80; }
			}
		}
	}
	
	ComputeOutDecimation();

//	if (BmpOut!=0) DiscardBitmap(BmpOut); BmpOut=0;

//	Status = NewBitmap (-1, PixDepth, WidthOut , HeightOut , NULL, BitsOut , NULL, &BmpOut );
//	if (Status!=0) { MessagePopup("Error!",GetUILErrorString (Status)); /*QuitUserInterface(1);*/ }
	Status = NewBitmap (-1, PixDepth, WidthOutD, HeightOutD, ColorTable, BitsOutD, NULL, &BmpOutD);
	if (Status!=0) { MessagePopup("Error!",GetUILErrorString (Status)); /*QuitUserInterface(1);*/ }

//			Status = SetBitmapData (BmpOut, WidthOut/PixDepth*8, PixDepth, ColorTable, BitsOut, NewMask);
	SetCtrlBitmap (Pnl, PNL_PIC, 0, BmpOutD);
	SetCtrlAttribute(Pnl, PNL_PIC, ATTR_DIMMED, FALSE);

	TrueSize();

	DiscardProgressDialog (Progress);
	SetWaitCursor (0); // Turn off wait cursor
}


int CVICALLBACK cb_Deform (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(Pnl, PNL_DEFORM, &Deform);
			SetDeform(Deform);
			break;
		case EVENT_RIGHT_CLICK:
   			MessagePopup("Normal/Deform", "Normal: view the picture loaded with the [Load] button.\n"
   										  "Deform: run the deformation (may take a while) and display the result.\n\n" 
   										  "Note: if the horizon is not a straight line after the deformation\n"
		   								  "      it means that one of the parameters is wrong (probably the resolution).");
			break;
	}
	return 0;
}


///////////////////////////////////////////////////////////////////////////////
static void SizeHoriz(void) {
	int Left, Top, Width, Height;
	GetCtrlAttribute (Pnl, PNL_PIC,   ATTR_LEFT,  &Left);
	GetCtrlAttribute (Pnl, PNL_PIC,   ATTR_TOP,   &Top);
	GetCtrlAttribute (Pnl, PNL_PIC,   ATTR_WIDTH, &Width);
	GetCtrlAttribute (Pnl, PNL_PIC,   ATTR_HEIGHT,&Height);
	SetCtrlAttribute (Pnl, PNL_HORIZ, ATTR_LEFT,  Left+Width/2);
	SetCtrlAttribute (Pnl, PNL_HORIZ, ATTR_TOP,   Top+2);
	SetCtrlAttribute (Pnl, PNL_HORIZ, ATTR_HEIGHT,Height-4);
}

int CVICALLBACK cbp_Panel (int panel, int event, void *callbackData,
		int eventData1, int eventData2) {
	int Width, Height, i;
	switch (event) {
		case EVENT_PANEL_SIZE:
			GetPanelAttribute(Pnl, ATTR_WIDTH, &Width);
			GetPanelAttribute(Pnl, ATTR_HEIGHT, &Height);
			if (Height<120) SetPanelAttribute(Pnl, ATTR_HEIGHT, Height=120);
			SetCtrlAttribute(Pnl, PNL_PIC, ATTR_WIDTH,  Width-5);
			SetCtrlAttribute(Pnl, PNL_PIC, ATTR_HEIGHT, Height-112);
			SizeHoriz();
			break;

		case EVENT_FILESDROPPED:
			i=0;
			while (((char**)eventData1)[i]!=NULL and ((char**)eventData1)[i][0]!='\0') {
				strcpy(FileName, ((char**)eventData1)[i]);
				if (!LoadFile()) return 0;
			}
			break;

		case EVENT_CLOSE:
			cb_Quit (Pnl, PNL_QUIT, EVENT_COMMIT, NULL, 0,0);
			break;
		}
	return 0;
}



static void SetFactor(double F) {
	Factor=F;
	SetCtrlVal(Pnl, PNL_FACTOR, Factor*DegToRad);
	SetCtrlVal(Pnl, PNL_NEW_WIDTH, WidthOut=floor(2*AlphaMax*Factor+0.5));
	SetCtrlVal(Pnl, PNL_NEW_HEIGHT, HeightOut=floor((PhiMax-PhiMin)*Factor+0.5));
}

int CVICALLBACK cb_Factor (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	double F;
	switch (event) {
		case EVENT_COMMIT:
		case EVENT_VAL_CHANGED:
			GetCtrlVal(Pnl, PNL_FACTOR, &F);
			SetFactor(F*RadToDeg);
			break;
		case EVENT_RIGHT_CLICK:
   			MessagePopup("Factor", "In Pix/deg.\nConverts from Alpha/Phi angles to WidthOut/HeightOut pixels."
   						"A larger value than the default will tend to enlarge the picture."
   						"\nWhen doing multiple pictures with different angles, you should try to"
   						"\nkeep this value the same for all pictures.");
			break;
	}
	return 0;
}


///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_CopyToClipboad (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			SetWaitCursor(1);
			Status = NewBitmap (-1, PixDepth, WidthOut , HeightOut , NULL, BitsOut , NULL, &BmpOut );
			if (Status!=0) { MessagePopup("Error!",GetUILErrorString (Status)); goto Out; }
			Status=ClipboardPutBitmap (BmpOut);
			if (Status!=0) { MessagePopup("Error!",GetUILErrorString (Status)); goto Out; }
			DiscardBitmap (BmpOut);
			
			Out:	SetWaitCursor(0);
			break;
		case EVENT_RIGHT_CLICK:
   			MessagePopup("Copy", "Copy the deformed picture to the clipboard.[Ctrl][C] shortcut.");
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
static void SetDefaultColor(long D) {
	DefaultColor=D;
	SetCtrlVal(Pnl, PNL_DEFAULT_COLOR, DefaultColor);
	DCr=DefaultColor>>16;
	DCg=(DefaultColor>>8) & 0xFF;
	DCb=DefaultColor & 0xFF;
}

int CVICALLBACK cb_DefaultColor (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int D;
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(panel, control, &D);
			SetDefaultColor(D);
			break;
		case EVENT_RIGHT_CLICK:
   			MessagePopup("Background color", "Color (hex RRGGBB) used to fill the unmapped areas.\n"
   											"0 is black,\n"
   											"FFFFFF is white,\n"
   											"808080 is grey,\n"
   											"FF0000 is red, etc...");
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_10Line (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int Res;
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(Pnl, PNL_10LINE, &DoLine);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup ("Draws 10� lines", "Draws lines at 10� interval on the resulting image.\n"
						"This can be use for trial\n"
						"if you are not sure about you lense focal length\n"
						"or other options.");
			break;
	}
	return 0;
}


///////////////////////////////////////////////////////////////////////////////
// This is the reduction factor for the image to display
static int ComputeInDecimation(void) {
	GetCtrlAttribute (Pnl, PNL_HELP, ATTR_LEFT, &MinWidth);
	DecimationIn=WidthIn/MinWidth;
	if (DecimationIn<1) DecimationIn=1;
	
	WidthInD = (WidthIn+DecimationIn-1)/DecimationIn;		// ??? +1
	HeightInD = (HeightIn+DecimationIn-1)/DecimationIn;
	RowByteInD = WidthInD*PixDepth/8;	// RowByteIn/DecimationIn+1;
	
	if (BitsInD!=NULL) free(BitsInD);
	BitsInD=malloc(RowByteInD*HeightInD);
	
//	DecimateRGB32( (DWORD*)BitsIn, WidthIn, HeightIn, HeightIn*RowByteIn, (DWORD*)BitsInD, DecimationIn);
	DecimateImage( BitsIn, WidthIn, HeightIn, PixDepth, BitsInD, DecimationIn);
	
	return DecimationIn;
}

static int ComputeOutDecimation(void) {
	GetCtrlAttribute (Pnl, PNL_HELP, ATTR_LEFT, &MinWidth);
	DecimationOut=WidthOut/MinWidth;
	if (DecimationOut<1) DecimationOut=1;
	
	WidthOutD = (WidthOut+DecimationOut-1)/DecimationOut;		// ??? +1
	HeightOutD = (HeightOut+DecimationOut-1)/DecimationOut;
	RowByteOutD = WidthOutD*PixDepth/8;	// RowByteOut/DecimationOut+1;
	
	if (BitsOutD!=NULL) free(BitsOutD);
	BitsOutD=malloc(RowByteOutD*HeightOutD);
	
//	DecimateRGB32( (DWORD*)BitsOut, WidthOut, HeightOut, HeightOut*RowByteOut, (DWORD*)BitsOutD, DecimationOut);
	DecimateImage( BitsOut, WidthOut, HeightOut, PixDepth, BitsOutD, DecimationOut);
	
	return DecimationOut;
}



static void TrueSize(void) {
	int PicWidth, PicHeight;
	GetCtrlAttribute (Pnl, PNL_HELP, ATTR_LEFT, &MinWidth);
	if (Deform) {
		if (WidthOutD>=MinWidth) {
			PicHeight=HeightOutD+6;	// Because border is 3px thick
			PicWidth=WidthOutD+6;
		} else if (WidthOutD>0) {					// image too small
			PicWidth=MinWidth+6;
			PicHeight=MinWidth*HeightOutD/WidthOutD+6;	// Because border is 3px thick
		} else {
			PicWidth=MinWidth+6;
			PicHeight=MinWidth*2/3+6;	// Because border is 3px thick
		}
	}
	else {
		if (WidthInD>=MinWidth) {
			PicHeight=HeightInD+6;
			PicWidth=WidthInD+6;
		} else if (WidthInD>0) {					// image too small
			PicWidth=MinWidth+6;
			PicHeight=MinWidth*HeightInD/WidthInD+6;	// Because border is 3px thick
		} else {
			PicWidth=MinWidth+6;
			PicHeight=MinWidth*2/3+6;	// Because border is 3px thick
		}
	}
	SetCtrlAttribute (Pnl, PNL_PIC, ATTR_WIDTH, PicWidth);
	SetCtrlAttribute (Pnl, PNL_PIC, ATTR_HEIGHT, PicHeight);
	SetPanelAttribute(Pnl, ATTR_WIDTH,  PicWidth+5);
	SetPanelAttribute(Pnl, ATTR_HEIGHT, PicHeight+112);
	SizeHoriz();
}

int CVICALLBACK cb_TrueSize (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			TrueSize();
			break;
		case EVENT_RIGHT_CLICK:
   			MessagePopup("TrueSize", "Set the size of the picture to its true pixel size.\n"
   									 "Warning: do not use it on big pictures if you don't want\n"
   									 "to go out of the screen\n"
   									 "You can always resize the window.");
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_WH (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_RIGHT_CLICK:
   			MessagePopup("Width/Height", "Width/height of the original picture in pixel.\n"
   										 "LenX/LenY: lenght of the original picture in mm (should be close to 24x36).");
			break;
	}
	return 0;
}

int CVICALLBACK cb_HW2 (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_RIGHT_CLICK:
   			MessagePopup("NewW/NewH", "NewW/NewH: width/height of the deformed picture in pixels.\n"
   									  "AlphaMax: horizontal view angle spans from -AlphaMax to +AlphaMax.\n"
   									  "Phi: vertical view angle spans from PhiMin to PhiMax.\n"
   									  "Factor: converts from Alpha/Phi angles to NewW/NewH pixels.");
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_PasteFromClipboad (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	static BOOL FirstCall=TRUE;
	switch (event) {
		case EVENT_COMMIT:
			if (FirstCall)
				MessagePopup("Warning !",
							"A unidentified bug in the paste function looses some colors on paste."
							"\nYou can use it, but the result won't look as good as if you read from a file."
							"\nAlso the size of the images you can paste is somewhat limited.");
			FirstCall=FALSE;
			SetWaitCursor (1); // Turn on wait cursor
			GetBitmap(ORIG_PASTE, "");
			SetWaitCursor (0);
			break;
		case EVENT_RIGHT_CLICK:
   			MessagePopup("Paste", "Paste a normal picture from the clipboard.\n[Ctrl][V] shortcut.");
			break;
	}
	return 0;
}


///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_Help (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int Res;
	switch (event) {
		case EVENT_COMMIT:
		case EVENT_LEFT_CLICK:
			Res = LaunchExecutable ("EXPLORER.EXE http://www.gdargaud.net/Hack/PanoWarp.html");
			//if (Res!=0) MessagePopup("Error!",GetUILErrorString (Status));
			break;
	}
	return 0;
}

