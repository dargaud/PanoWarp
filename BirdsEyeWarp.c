#include <windows.h>
#include <ansi_c.h>
#include <utility.h>
#include <cvirte.h>		
#include <userint.h>

#include "Def.h"
#include "ReadSavePng.h"
#include "ReadSaveJpeg.h"

#include "PanoWarp.h"

#define _VER_ "1.1"			// Version
#define _WEB_ "http://www.gdargaud.net/Hack/PanoWarp.html"
#define _COPY_ "� 2006 Guillaume Dargaud (freeware)"

static char *ShortInfo="BirdsEyeWarp v" _VER_ " - " _COPY_ " " _WEB_;

static char *Info="BirdsEyeWarp v" _VER_ " - " _COPY_ " - Last compiled on " __DATE__ "\n\n"
			"This program transforms full (360x180�) panoramic images into 360� fisheye-like images.\n"
			"In other words, _double_ the field of view of a normal 8mm fisheye lens.\n\n"
			"The input image should be produced with a panorama assembly program\n"
			"from several fisheye images and should have a 360� horizontal\n"
			"and 180� vertical field of view.\n"
			"Right-click on any control for further help\n"
			"Tutorial at " _WEB_;
   						
static int Pnl;
static int BmpIn=0, BmpOut=0;
static unsigned char *BitsIn=NULL, *BitsOut=NULL;
static int RowBytes, Height, Width, PixDepth;	// Input image
static int RowBytesOut, HeightOut, WidthOut, PixDepthOut;	// Output image
static int cWidth, cHeight, cTop, cLeft;		// Canvas size

static double Gap=0;
static int ColorRef=0xFF4444, ColorLine=0x4444FF;
static double Rotation=0;	// For the rotation angle
static double ZoomX=1, ZoomY=1;
static const double ArbitraryAngle0=DegToRad*30, 	// Default position of the border points
					ArbitraryAngle1=DegToRad*(180-30), 
					ArbitraryAngle2=DegToRad*(180+30);

static BOOL T=FALSE,	// Whether the image is transformed or not
			Vertical=FALSE,	// If the camera was pointing vertically or horizontally
			CanvasFocused=FALSE;	// If the focus is on the canvas or not

static char Drive[MAX_DRIVENAME_LEN], Dir[MAX_DIRNAME_LEN], FileName[MAX_PATHNAME_LEN];

static BOOL LoadFile(void);
static void DrawLine(void);


///////////////////////////////////////////////////////////////////////////////
int main (int argc, char *argv[]) {
	if (InitCVIRTE (0, argv, 0) == 0)
		return -1;	/* out of memory */
	if ((Pnl = LoadPanel (0, "PanoWarp.uir", BE)) < 0)
		return -1;

	DisplayPanel (Pnl);
	SetCtrlVal(Pnl, BE_TEXTMSG, ShortInfo);
	cbp_Panel (Pnl, EVENT_PANEL_SIZE, NULL, 0, 0);
	#ifndef _CVI_DEBUG_
	cbp_Panel (Pnl, EVENT_RIGHT_CLICK, NULL, 0, 0);
	#endif
	
	// catch dragged-and-dropped file
	EnableDragAndDrop (Pnl);
	

	///////////////////
	RunUserInterface();
	///////////////////
	
	DisableDragAndDrop (Pnl);
	DiscardPanel (Pnl);
	return 0;
}


///////////////////////////////////////////////////////////////////////////////
// Set title of the panel with filename and zoom factor
void SetTitle(void) {
	char Title[MAX_PATHNAME_LEN+100];
	if (FileName==NULL or FileName[0]=='\0') {
		sprintf(Title, "Zoom %.0f - Birdseye warping", ZoomX);
	} else {
		SplitPath (FileName, Drive, Dir, Title);
		memmove (Dir+strlen(Drive), Dir, strlen(Dir)+1);
		memmove (Dir, Drive, strlen(Drive));		// We now have the path inside Dir
		sprintf(Title, "%s - Zoom 1:%.0f - Birdseye warping", Title, ZoomX);
	}	
	SetPanelAttribute (Pnl, ATTR_TITLE, Title);
}


///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cbp_Panel (int panel, int event, void *callbackData,
		int eventData1, int eventData2) {
	typedef struct {
		unsigned int uMsg; // Windows message code
		unsigned int wParam;
		long lParam;
		long result;
	} InterceptedWindowsMsg;
	InterceptedWindowsMsg *msg;
	int X, Y, Left, Top;
	int pWidth, pHeight, sWidth, sHeight, i;
	
	switch (event) {
		case EVENT_PANEL_SIZE:
			GetPanelAttribute(Pnl, ATTR_HEIGHT, &pHeight);
			GetPanelAttribute(Pnl, ATTR_WIDTH, &pWidth);
//			GetScreenSize (&sHeight, &sWidth);
//			pHeight=Min(pHeight,sHeight);
//			pWidth=Min(pWidth,sWidth);
			
			GetCtrlAttribute(Pnl, BE_CANVAS, ATTR_LEFT,&cLeft);
			GetCtrlAttribute(Pnl, BE_CANVAS, ATTR_TOP, &cTop);
			SetCtrlAttribute(Pnl, BE_TRANSPARENT, ATTR_LEFT,cLeft);
			SetCtrlAttribute(Pnl, BE_TRANSPARENT, ATTR_TOP, cTop);
//			if (pHeight<120) SetPanelAttribute(Pnl, ATTR_HEIGHT, pHeight=120);
			if (Width==0 or Height==0) { Width=400; Height=300; }
			#define CurrentImageWidth (T?WidthOut:Width)
			#define CurrentImageHeight (T?HeightOut:Height)
			cWidth=pWidth-cLeft;
			cHeight=pHeight-cTop;
			ZoomX=(double)CurrentImageWidth/cWidth;
			ZoomY=(double)CurrentImageHeight/cHeight;
			ZoomX=ZoomY=Max(ZoomX, ZoomY);	
			SetCtrlAttribute(Pnl, BE_CANVAS, ATTR_WIDTH,  cWidth=RoundRealToNearestInteger(CurrentImageWidth/ZoomX));
			SetCtrlAttribute(Pnl, BE_CANVAS, ATTR_HEIGHT, cHeight=RoundRealToNearestInteger(CurrentImageHeight/ZoomY));
			SetCtrlAttribute(Pnl, BE_TRANSPARENT, ATTR_WIDTH,  cWidth);
			SetCtrlAttribute(Pnl, BE_TRANSPARENT, ATTR_HEIGHT, cHeight);
			SetPanelSize(Pnl, pHeight=cHeight+cTop, pWidth=cWidth+cLeft);
			SetTitle();
			if ((T ? BmpOut : BmpIn)!=0) 
				CanvasDrawBitmap (Pnl, BE_CANVAS, T ? BmpOut : BmpIn, VAL_ENTIRE_OBJECT, VAL_ENTIRE_OBJECT);
			break;
		
		case EVENT_FILESDROPPED:
			i=0;
			while (((char**)eventData1)[i]!=NULL and ((char**)eventData1)[i][0]!='\0') {
				strcpy(FileName, ((char**)eventData1)[i]);
				if (!LoadFile()) return 1;
				i++;
			}
			break;
		
		case EVENT_WINDOWS_MSG:
			msg = (InterceptedWindowsMsg *)eventData1;
			if (/*msg->wParam and*/ /*CanvasFocused and*/ 
				( msg->uMsg==WM_LBUTTONDOWN or msg->uMsg==WM_LBUTTONUP ) ) {
				GetGlobalMouseState (NULL, &X, &Y, NULL, NULL, NULL);
				GetPanelAttribute (Pnl, ATTR_LEFT, &Left);
				GetPanelAttribute (Pnl, ATTR_TOP, &Top);
				// send custom mouse-down or mouse-up event
				cb_Canvas (Pnl, BE_TRANSPARENT, msg->uMsg, NULL, Y-Top, X-Left);
    		}
			break;
		
		case EVENT_RIGHT_CLICK:
   			MessagePopup("BirdsEyeWarp Help", Info);
			return 1;
	
		case EVENT_KEYPRESS:
			if (eventData1==VAL_F1_VKEY) {
	   			MessagePopup("BirdsEyeWarp Help", Info);
				return 1;
			}
	}
	return 0;
}



///////////////////////////////////////////////////////////////////////////////
// Set the size respecting the ratio, if it fits
// TODO: size for Out
void SetCanvasSize(void) {
	int ScrHeight, ScrWidth, Left, Top, pLeft, pTop;
	int Zoom;
	GetScreenSize (&ScrHeight, &ScrWidth);
	GetPanelAttribute(Pnl, ATTR_LEFT, &pLeft);
	GetPanelAttribute(Pnl, ATTR_TOP, &pTop);
	
	ZoomX=ZoomY=Zoom=Max(1, 1+sqrt(CurrentImageHeight*CurrentImageHeight+CurrentImageWidth*CurrentImageWidth)/
						sqrt((ScrHeight-pTop)*(ScrHeight-pTop)+(ScrWidth-pLeft)*(ScrWidth-pLeft)) );

	SetCtrlAttribute(Pnl, BE_CANVAS, ATTR_WIDTH,  cWidth=CurrentImageWidth/ZoomX);
	SetCtrlAttribute(Pnl, BE_CANVAS, ATTR_HEIGHT, cHeight=CurrentImageHeight/ZoomY);
	SetCtrlAttribute(Pnl, BE_TRANSPARENT, ATTR_WIDTH,  cWidth);
	SetCtrlAttribute(Pnl, BE_TRANSPARENT, ATTR_HEIGHT, cHeight);
	GetCtrlAttribute(Pnl, BE_CANVAS, ATTR_LEFT,  &Left);
	GetCtrlAttribute(Pnl, BE_CANVAS, ATTR_TOP, &Top);
	SetPanelAttribute(Pnl, ATTR_WIDTH, CurrentImageWidth/Zoom+Left);
	SetPanelAttribute(Pnl, ATTR_HEIGHT, CurrentImageHeight/Zoom+Top);
	SetTitle();
	if ((T ? BmpOut : BmpIn)!=0) 
		CanvasDrawBitmap (Pnl, BE_CANVAS, T ? BmpOut : BmpIn, VAL_ENTIRE_OBJECT, VAL_ENTIRE_OBJECT);
}


///////////////////////////////////////////////////////////////////////////////
// Various UIR changes depending if image is original or transformed
void Transformed(void) {
	SetCanvasSize();
	SetCtrlAttribute(Pnl, BE_REVERSE, ATTR_DIMMED, T);
	SetCtrlAttribute(Pnl, BE_COPY, ATTR_DIMMED, !T);
	SetCtrlAttribute(Pnl, BE_SAVE, ATTR_DIMMED, !T);
	SetCtrlAttribute(Pnl, BE_DEFORM, ATTR_DIMMED, FALSE);
	SetCtrlAttribute(Pnl, BE_ROTATION, ATTR_DIMMED, T);
	SetCtrlVal(Pnl, BE_DEFORM, T);
	if ((T ? BmpOut : BmpIn)!=0) 
		CanvasDrawBitmap (Pnl, BE_CANVAS, T ? BmpOut : BmpIn, VAL_ENTIRE_OBJECT, VAL_ENTIRE_OBJECT);
	SetCtrlAttribute(Pnl, BE_TRANSPARENT, ATTR_VISIBLE, !T);
}


//
static void FinishSettingUp(void) {
	Gap=(Width/2-Height)/2;	// in case of non conform image
	if (!BETWEEN(190,100*Width/Height,210)) MessagePopup("Warning: inaccurate ratio",
		Gap>0 ?
		"The starting image should have a 2:1 (=360:180) H/V ratio\n"
		"Let's see what we can do anyway.\n"
		"The horizon is assumed to be in the middle of the input pano\n"
		"and a gap will be left in the center of the resulting bird's eye image."
		:
		"The starting image should have a 2:1 (=360:180) H/V ratio\n"
		"Let's see what we can do anyway.\n"
		"The horizon is assumed to be in the middle of the input pano\n"
		"And the input pano will be stretched vertically to fit."
		);
	Gap=MAX(Gap,0);
	SetCanvasSize();
		
	T=FALSE;
	Transformed();
	DrawLine();
}



///////////////////////////////////////////////////////////////////////////////
// Return TRUE if file not readable
static BOOL LoadFile(void) {
	int Status, PreviousHeight, PreviousWidth;
	PreviousHeight=Height; PreviousWidth=Width;
	SetWaitCursor(1);
	SetTitle();
	
	if (BmpIn!=0) DiscardBitmap (BmpIn); BmpIn=0;

	if (toupper(FileName[strlen(FileName)-3])=='P' and
		toupper(FileName[strlen(FileName)-2])=='N' and
		toupper(FileName[strlen(FileName)-1])=='G') 
		Status = Png_ReadBitmapFromFile (&BmpIn, &BitsIn, &RowBytes, &PixDepth, &Width, &Height, FileName);
	else
	if (toupper(FileName[strlen(FileName)-3])=='J' and
		toupper(FileName[strlen(FileName)-2])=='P' and
		toupper(FileName[strlen(FileName)-1])=='G' or
		toupper(FileName[strlen(FileName)-4])=='J' and
		toupper(FileName[strlen(FileName)-3])=='P' and
		toupper(FileName[strlen(FileName)-2])=='E' and
		toupper(FileName[strlen(FileName)-1])=='G') 
		Status = Jpg_ReadBitmapFromFile (&BmpIn, &BitsIn, &RowBytes, &PixDepth, &Width, &Height, FileName);
	else {
		MessagePopup("Error", "Selected file is not readable");
		return TRUE;
	}
	
	SetWaitCursor(0);
	if (Status) MessagePopup("Error", "Selected file is not readable");
//			else SetCanvasSize();
	
	FinishSettingUp();
	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_Load (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	char Title[MAX_PATHNAME_LEN+100];
	int Status;
	
	switch (event) {
		case EVENT_COMMIT:
			Status = FileSelectPopup ("", "*.png;*.jpg;*.jpeg", "*.png;*.jpg;*.jpeg",
									  "Open png/jpg file", VAL_LOAD_BUTTON,
									  0, 0, 1, 1, FileName);
			if (Status!=VAL_EXISTING_FILE_SELECTED) return 0;
			LoadFile();			
			break;
		case EVENT_RIGHT_CLICK:
   			MessagePopup("Load", "Selects and loads a PNG or JPG image file.");
			return 1;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_Save (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	char Title[MAX_PATHNAME_LEN+100], Response[5]="75";
	static BOOL FirstCallPng=TRUE, FirstCallJpg=TRUE;
	static int CompressionJpeg=75, Status;
	time_t Time=time(NULL);
	char *Dot, Sample[MAX_PATHNAME_LEN];

	switch (event) {
		case EVENT_COMMIT:
			if (strlen(FileName)==0) strcpy(FileName,"PanoToBirdsEye.png");
			Dot=strrchr (FileName, '.');
			strncpy(Sample, FileName, Dot-FileName);
			Sample[Dot-FileName]='\0';
			strcat(Sample, "W.png");
			
			Status = FileSelectPopup (Dir, Sample, "*.png;*.jpg;*.jpeg",
									  "Save warped image to ?", VAL_SAVE_BUTTON,
									  0, 0, 1, 1, FileName);
			if (Status!=VAL_EXISTING_FILE_SELECTED and Status!=VAL_NEW_FILE_SELECTED) return 0;
			SetWaitCursor(1);
			
			if (toupper(FileName[strlen(FileName)-3])=='P' and
				toupper(FileName[strlen(FileName)-2])=='N' and
				toupper(FileName[strlen(FileName)-1])=='G') {
				if (FirstCallPng) {
					Png_SetParameters (0.45455, 0, 28, Z_DEFAULT_COMPRESSION, 0);
					Png_SetTextTitle("Bird's eye view", FALSE);
					Png_SetTextDescription("Image warped from original 360x180� spherical panorama into a total 180� fisheye image", FALSE);
					Png_SetTextCopyright("Image � ??? - Warping software " _COPY_, FALSE);
					Png_SetTextCreationTime(ctime(&Time), FALSE);
					Png_SetTextSoftware(ShortInfo, FALSE);
					Png_SetTextComment("This can be used to give a 'planet earth' effect to an already assembled panorama.", FALSE);
					FirstCallPng=FALSE;
				}
				Png_SaveBitmapToFile (BitsOut, RowBytesOut, PixDepth, WidthOut, HeightOut, FileName);
			}
			else
			if ((toupper(FileName[strlen(FileName)-3])=='J' and
				 toupper(FileName[strlen(FileName)-2])=='P' and
				 toupper(FileName[strlen(FileName)-1])=='G') or 
				(toupper(FileName[strlen(FileName)-4])=='J' and
				 toupper(FileName[strlen(FileName)-3])=='P' and
				 toupper(FileName[strlen(FileName)-2])=='E' and
				 toupper(FileName[strlen(FileName)-1])=='G') ) {
				if (FirstCallJpg) {
					PromptPopup ("JPEG compression quality ?",
								 "Please choose the compression ratio for the JPEG compression.\n"
								 "1=high compression, very poor quality\n"
								 "100=no compression, highest quality\n"
								 "75=default compression, good quality",
								 Response, 4);
					CompressionJpeg=atoi(Response);
					if (strlen(Response)==0) CompressionJpeg=75;
					if (CompressionJpeg<1) CompressionJpeg=75;
					if (CompressionJpeg>100) CompressionJpeg=75;
					FirstCallJpg=FALSE;
				}
				Jpg_SaveBitmapToFile (BitsOut, RowBytesOut, PixDepth, WidthOut, HeightOut, FileName, CompressionJpeg);
			} 
			else MessagePopup("Error!", "Error, invalid file extension specified.");

			SetWaitCursor (0);
			break;
		case EVENT_RIGHT_CLICK:
   			MessagePopup("Load", "Save warped image to a compressed png or jpg file.");
			return 1;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_PasteFromClipboad (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	static BOOL FirstCall=TRUE;
	int Error, Status, *ColorTable, PreviousHeight, PreviousWidth;
	unsigned char *Mask;
	BOOL Available;
	
	switch (event) {
		case EVENT_COMMIT:
			#ifndef _CVI_DEBUG_
			if (FirstCall)
				MessagePopup("Warning !",
							"A unidentified bug in the paste function looses some colors when pasting."
							"\nYou can use it, but the result won't look as good as if you read from a file."
							"\nAlso the size of the images you can paste is somewhat limited.");
			#endif
			FirstCall=FALSE;
			SetWaitCursor (1); // Turn on wait cursor

			strcpy(FileName, ""); SetTitle();
			if (BmpIn!=0) DiscardBitmap (BmpIn); BmpIn=0;
			Error=ClipboardGetBitmap (&BmpIn, &Available);
			
			SetWaitCursor (0);
			if (Error!=0) {
				MessagePopup ("Error !", GetUILErrorString(Error));
				return 0;
			} else if (!Available) {
				MessagePopup ("Warning !", "The clipboard does not seem to contain a valid image.");
				return 0;
			}

			Status = AllocBitmapData (BmpIn, &ColorTable, &BitsIn, &Mask);
			if (Status!=0) { MessagePopup("Error!",GetUILErrorString (Status)); /*QuitUserInterface(1);*/ }
			if (ColorTable!=NULL) {	
				MessagePopup ("Warning !", "This program operates only on\n16 million color files.\nExiting.");
				return 0; 
			}
			if (Mask!=NULL) {	
				MessagePopup ("Warning !", "This program operates only on\nnon masked files.\nExiting.");
				return 0; 
			}
	
			PreviousHeight=Height; PreviousWidth=Width;
			Status = GetBitmapData (BmpIn, &RowBytes, &PixDepth, &Width, &Height, ColorTable, BitsIn, Mask);
			
			FinishSettingUp();
			
			break;
		
		case EVENT_RIGHT_CLICK:
   			MessagePopup("Paste", "Paste a normal picture from the clipboard\n"
   								"(16-bit images or paletted images not accepted).\n"
   								"Keyboard shortcut: [Ctrl][V]");
			return 1;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_CopyToClipboad (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int Status;
	switch (event) {
		case EVENT_COMMIT:
			SetWaitCursor(1);
			Status=ClipboardPutBitmap (BmpOut);
			if (Status!=0) { MessagePopup("Error!",GetUILErrorString (Status)); }
			SetWaitCursor(0);
			break;
		case EVENT_RIGHT_CLICK:
   			MessagePopup("Copy", "Copy the deformed picture to the clipboard.[Ctrl][C] shortcut.");
			return 1;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_Quit (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			QuitUserInterface (0);
			break;
		case EVENT_RIGHT_CLICK:
   			MessagePopup("Help Quit", "What do you seriously expect to see here ?!?");
			return 1;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////

int CVICALLBACK cb_Deform (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	unsigned char *Source, *Dest, Color[4];
	int Progress;
	int x, y, Xs, Ys, Xd, Xc, Yc;
	double Yr, r, Rd, Alpha;
	double cR, sR;
	BOOL Reverse;
	unsigned long TopAvg[4], BotAvg[4];
	
	cR=cos(Rotation); sR=sin(Rotation);

	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(Pnl, BE_DEFORM, &T);
			GetCtrlVal(Pnl, BE_REVERSE, &Reverse);
			if (T) {
				SetWaitCursor(TRUE);
				// HeightOut=WidthOut=Height*2;	// Square destination image, assuming Height=Width/2
				HeightOut=WidthOut=(Height+Gap)*2;	// Square destination image
				RowBytesOut=WidthOut*PixDepth/8;
				if (BmpOut!=0) DiscardBitmap (BmpOut); BmpOut=0;
				BitsOut=realloc(BitsOut, HeightOut*RowBytesOut*PixDepth/8);

				Progress = CreateProgressDialog ("Warping", "Percent Complete", 1, VAL_FULL_MARKERS, "Cancel");

				
				// Set default missing color
				// memset (BitsOut, 0, HeightOut*RowBytesOut*PixDepth/8);
				TopAvg[0]=TopAvg[1]=TopAvg[2]=TopAvg[3]=0;
				BotAvg[0]=BotAvg[1]=BotAvg[2]=BotAvg[3]=0;
				
				// Average color of the top and bottom lines
				for (x=0; x<Width; x++) {
					Source=&BitsIn[x*PixDepth/8];
					TopAvg[0]+=*Source;
					if (PixDepth>8) {
						TopAvg[1]+=*++Source;
						TopAvg[2]+=*++Source;
						if (PixDepth==32)
							TopAvg[3]+=*++Source;
					}

					Source=&BitsIn[x*PixDepth/8+(Height-1)*RowBytes];
					BotAvg[0]+=*Source;
					if (PixDepth>8) {
						BotAvg[1]+=*++Source;
						BotAvg[2]+=*++Source;
						if (PixDepth==32)
							BotAvg[3]+=*++Source;
					}
				}
				TopAvg[0]/=Width;	BotAvg[0]/=Width;
				TopAvg[1]/=Width;	BotAvg[1]/=Width;
				TopAvg[2]/=Width;	BotAvg[2]/=Width;
				TopAvg[3]/=Width;	BotAvg[3]/=Width;
				
				for (x=0; x<WidthOut; x++) {		// Pre-fill image
					if (UpdateProgressDialog (Progress, 50*x/WidthOut, 0)) {
						T=FALSE; Transformed(); return 0; }		// Manual abort
					for (y=0; y<HeightOut; y++) {
						if ( ((x-WidthOut/2)*(x-WidthOut/2)+(y-HeightOut/2)*(y-HeightOut/2)<HeightOut*HeightOut/4) 
							xor Reverse) {
							Color[0]=BotAvg[0];
							Color[1]=BotAvg[1];
							Color[2]=BotAvg[2];
							Color[3]=BotAvg[3];
						} else {
							Color[0]=TopAvg[0];
							Color[1]=TopAvg[1];
							Color[2]=TopAvg[2];
							Color[3]=TopAvg[3];
						}
						Dest=&BitsOut[x*PixDepth/8+y*RowBytesOut];
						*Dest=Color[0];
						if (PixDepth>8) {
							*++Dest=Color[1];
							*++Dest=Color[2];
							if (PixDepth==32)
								*++Dest=Color[3];
						}
					}
				}
				
				
				// Now let's rip
				for (x=0,Xc=-WidthOut/2; x<WidthOut; x++,Xc++) {
					if (UpdateProgressDialog (Progress, 50+50*x/WidthOut, 0)) {
						T=FALSE; Transformed(); return 0; }		// Manual abort
					for (y=0,Yc=-HeightOut/2; y<HeightOut; y++,Yc++) {
						r=sqrt(Xc*Xc+Yc*Yc);	// Xc/Yc are centered into the fisheye image
						Alpha=atan2(Yc,Xc);		// This is the core transform formula
						Xs=fmod(Width*(Rotation+PI/2+(Reverse?-Alpha:Alpha))/(2*PI)+2*Width, Width);
						Ys=( Reverse ? r-Gap : Height-1-r+Gap );
						if (!BETWEEN(0,Xs,Width-1) or !BETWEEN(0,Ys,Height-1) ) 
							continue;
						Source=&BitsIn[Xs*PixDepth/8+Ys*RowBytes];
						Dest=&BitsOut[x*PixDepth/8+y*RowBytesOut];
						*Dest=*Source;
						if (PixDepth>8) {
							*++Dest=*++Source;
							*++Dest=*++Source;
							if (PixDepth==32)
								*++Dest=*++Source;
						}
					}
				}
				DiscardProgressDialog (Progress);
				if (0>NewBitmap (RowBytesOut, PixDepth, WidthOut, HeightOut, NULL, BitsOut, NULL, &BmpOut)) {
					MessagePopup("Error", "Not enough memory to display the image,\nbut you can probably still try to save it.");
				}
			}
			Transformed();
			SetWaitCursor(FALSE);
			break;
		
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help Deform", "Deform the original panoramic image into a 360� fisheye image");
			return 1;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// Position break line
static void DrawLine(void) {
	int Xbreak;
	Xbreak=Width*Rotation/(2*PI);

	CanvasClear (Pnl, BE_TRANSPARENT, VAL_ENTIRE_OBJECT);
	SetCtrlAttribute (Pnl, BE_TRANSPARENT, ATTR_PEN_MODE, VAL_COPY_MODE);
	SetCtrlAttribute(Pnl, BE_TRANSPARENT, ATTR_PEN_COLOR, ColorLine);
	SetCtrlAttribute(Pnl, BE_TRANSPARENT, ATTR_PEN_FILL_COLOR, ColorLine);
	SetCtrlAttribute(Pnl, BE_TRANSPARENT, ATTR_PEN_WIDTH, 1);

	// Draw vertical line
	CanvasDrawLine (Pnl, BE_TRANSPARENT, MakePoint(Xbreak/ZoomX, 0), MakePoint(Xbreak/ZoomX, Height/ZoomY));
}


int CVICALLBACK cbwin_Canvas(int panelHandle, int message, unsigned int* wParam, 
					unsigned int* lParam, void* callbackData) {
	switch (message) {
		case EVENT_NEWHANDLE: ;
	}
	return 0;
}


int CVICALLBACK cb_Canvas (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int X, Y, Xbreak;
	
	switch (event) {
		case EVENT_GOT_FOCUS: CanvasFocused=TRUE;  break;
		case EVENT_LOST_FOCUS:CanvasFocused=FALSE; break;
		
		case EVENT_LEFT_CLICK:
			X=eventData2-cLeft;
			Y=eventData1-cTop;
			if (BmpIn==0 or Width==0 or Height==0) return 0;
			
			Xbreak=ZoomX*X;

			Rotation=2*PI*Xbreak/Width;
			SetCtrlVal(Pnl, BE_ROTATION, RadToDeg*Rotation);
			
			DrawLine();
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help Canvas", "Move the vertical line to set the position of the\n"
										"upper part of the resulting fisheye image.");
			return 1;
	}
	return 0;
}

int CVICALLBACK cb_Rotation (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(Pnl, BE_ROTATION, &Rotation);
			Rotation*=DegToRad;
			DrawLine();
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help Center", "Set the rotation reference of the image\n(to try to keep the horizon horizontal)");
			return 1;
	}
	return 0;
}

int CVICALLBACK cb_Reverse (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help Reverse", "Normal setting is to have the bottom of the panorama\n"
						"converge at the center of the resulting circle.\n"
						"The other solution is to use the top of the panorama.");
			return 1;
	}
	return 0;
}
