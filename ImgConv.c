#include "ImgConv.h"

/******************************************************************************
  FUNCTION: ConvertRGBToMono
  PURPOSE: convert 32 bits image to 3 separate monochrome images (only for display)
  IN: pRGBImage: pointer to 32 bits image
      ImgSize: number of pixels of the image
      aMonoImages[3]: 3 pointers to separate monochrome 32 bits channels
  NOTE: monochrome bitmaps are still 32 bits bitmaps, so we have to triplicate each
        color channel, not only separate them.
******************************************************************************/
void ConvertRGBToMono( const DWORD * pRGBImage, const DWORD xSize, const DWORD ySize, DWORD ImgSize, 
					DWORD *pMono32[], const DWORD Decimation) {
	DWORD pixel;
    BYTE *pIn, *p0, *p1, *p2;
	int x,y,X,Y, pos;
	
	if (pRGBImage==NULL or pMono32==NULL) return;

    p0 = (BYTE*)pMono32[0]; 
    p1 = (BYTE*)pMono32[1]; 
    p2 = (BYTE*)pMono32[2]; 

	//SetCtrlVal(Pnl, PNL_MESSAGES, "\nEntering ConvertRGBToMono.");	
	
	if (Decimation==1)	// Faster loop
	    for ( pixel = 0; pixel<ImgSize; pixel++ ) {
	        pIn = (BYTE*)pRGBImage++; 
			// We want R=G=B on the resulting image
	        *p0++ = *p0++ = *p0++ = *p0++ = *pIn++; // *p0++=0;
	        *p1++ = *p1++ = *p1++ = *p1++ = *pIn++; // *p1++=0;
	        *p2++ = *p2++ = *p2++ = *p2++ = *pIn++; // *p2++=0;
	    }
	else 
	    for ( y=0, Y=0; y<ySize; y+=Decimation,Y++ ) {
		    for ( x=0, X=0; x<xSize; x+=Decimation,X++ ) {
		        pIn = (BYTE*)&(pRGBImage[x+y*xSize]); 
				// We want R=G=B on the resulting image
				pos=X+Y*Decimation;
				p0 = (BYTE*)&(pMono32[0][pos]);
				p1 = (BYTE*)&(pMono32[1][pos]);
				p2 = (BYTE*)&(pMono32[2][pos]);
		        *p0++ = *p0++ = *p0++ = *p0++ = *pIn++; // *p0++=0;
		        *p1++ = *p1++ = *p1++ = *p1++ = *pIn++; // *p1++=0;
		        *p2++ = *p2++ = *p2++ = *p2++ = *pIn++; // *p2++=0;
		    }
		}	
}


/******************************************************************************
  FUNCTION: DecimateRGB32
  PURPOSE: decimates 32 bits image
  IN: pRGBImage: pointer to 32 bits image
      ImgSize: number of pixels of the image
  OUT:pRGB32: pointer to decimated 32 bit RGB image
******************************************************************************/
void DecimateRGB32( DWORD * pRGBImage, const DWORD xSize, const DWORD ySize, DWORD ImgSize, 
				DWORD *pRGB32, const DWORD Decimation) {
	DWORD pixel;
    DWORD *pIn;
    DWORD *pOut=pRGB32;
	int x,y,X,Y;

	//SetCtrlVal(Pnl, PNL_MESSAGES, "\nEntering DecimateRGB32.");	

	if (pRGBImage==NULL or pRGB32==NULL) return;

	if (Decimation==1) 
		// Should use directly the 32 bit buffer instead of copying it
		memcpy(pRGB32, pRGBImage, ImgSize*sizeof(DWORD));
	else 
	    for ( y=0, Y=0; y<ySize; y+=Decimation,Y++ ) {
		    for ( x=0, X=0; x<xSize; x+=Decimation,X++ ) {
		        pIn = &(pRGBImage[x+y*xSize]); 
				// We want R=G=B on the resulting image
				pOut = &pRGB32[X+Y*Decimation];
		        *pOut = *pIn;	// red
		    }
		}	
}

/******************************************************************************
  FUNCTION: DecimateImage
  PURPOSE: decimates 8/16/24/32 bits image
  IN: pRGBImage: pointer to 8/16/24/32 bits image
      xSize, ySize: width and height
      PixDepth: 8/16/24/32
      Decimation: factor (1=duplicate image, 2=half...)
  OUT:pRGB32: pointer to decimated image
  NOTE: Size of decimated image is xSize/Decimate, ySize/Decimate
******************************************************************************/
void DecimateImage( unsigned char *InImage, 
				const int xSize, const int ySize, const int PixDepth, 
				unsigned char *OutImage, const int Decimation) {
	int BytesPerPix = PixDepth/8,	// 1/2/3/4
		xSizeD=(xSize+Decimation-1)/Decimation;
    unsigned char *pIn, *pOut;
	int x,y,X,Y;

	//SetCtrlVal(Pnl, PNL_MESSAGES, "\nEntering DecimateRGB32.");	

	if (InImage==NULL or OutImage==NULL) return;
	
	if (Decimation==1) 
		// Should use directly the 32 bit buffer instead of copying it
		memcpy(OutImage, InImage, xSize*ySize*BytesPerPix);
	else 
	    for ( y=0, Y=0; y<ySize; y+=Decimation,Y++ ) {
		    for ( x=0, X=0; x<xSize; x+=Decimation,X++ ) {
		        pIn  = & InImage [(x+y*xSize )*BytesPerPix]; 
				pOut = & OutImage[(X+Y*xSizeD)*BytesPerPix];

		        switch (BytesPerPix) {
		        	case 4:*pOut++ = *pIn++;	// no break;
		        	case 3:*pOut++ = *pIn++;
		        	case 2:*pOut++ = *pIn++;
		        	case 1:*pOut++ = *pIn++;
				}	        
		    }
		}	
}


