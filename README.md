PROGRAM:   PanoWarp.exe / FishEyeWarp.exe     / BirdsEyeWarp

VERSION:   2.5 / 1.5 / 1.1  

COPYRIGHT: � 1998-2006 Guillaume Dargaud - Freeware  

PURPOSE:   Deformation of bitmap pictures in order to stitch them in a panorama.  

INSTALL:   run SETUP.  
		   If setup fails, you may need to install the LabWindows/CVI runtime engine on your PC.  
		   It is available from the National Instrument website http://www.ni.com/  

HELP:      available by right-clicking an item on the user interface.  

TUTORIAL:  http://www.gdargaud.net/Hack/PanoWarp.html  
		   
KNOWN BUGS:- Sometimes after resizing an image, PaintShopPro will report an incorrect size;  
			 If you paste this incorrectly sized image into PanoWarp, you will get off-color  
			 display and then crappy warped pics. Not my bug.  

		   - Also, often when you paste an image, the system messes up the colors with a  
			 pixelation effect, barely visible, but it decreases the image quality (visible on sky).  
			 Better load from file instead of [Paste].  
			 The pixelation is in the image Windows gives me... Nothing I can do about it.  

		   - If you paste a 10000x10000 image, don't be surprised if it crashes.  

		   - Sometimes large images fail to display after transformation (out of memory).  
			 This is annoying by try to save it anyway, you don't see it but it's in memory.  

TODO:		Replace ReadSavePng/ReadSaveJpeg with internal LabWindows functions

NOTE:		PanoWarp is obsolete as there are now much better programs to assemble panoramic pictures
			(but in 1998 when I wrote it, there weren't)
			The other 2 programs are still useful in specific cases

LICENSE:   GPLv3
