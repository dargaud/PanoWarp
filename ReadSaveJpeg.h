// File:	ReadSaveJpeg.h
// Purpose:	Header file for ReadSaveJpeg.fp/lib
//			Saves bitmap, panel or control in a JPEG file
//			Reads a JPEG file to a bitmap or a picture control
// For help, see the help of the FP

#ifndef _READ_SAVE_JPEG
#define _READ_SAVE_JPEG

#ifndef TRUE
#define TRUE 1
#define FALSE 0
#endif


extern BOOL Jpg_SaveBitmapToFile(unsigned char * Bits,
					int RowBytes, int PixDepth, 
					const int Width, const int Height,
					const char* FileName, int quality);
					
extern BOOL Jpg_ReadBitmapFromFile(int *Bitmap, unsigned char **Bits,
					int *RowBytes, int *PixDepth, 
					int *Width, int *Height,
					const char* FileName);

extern BOOL Jpg_GetProperties(int *RowBytes, int *PixDepth, int *NbChan,
					int *Width, int *Height,
					const char* FileName);

#ifdef _CVI_
extern BOOL Jpg_SavePanelControlToFile(int Panel, int Control,
					const char* FileName, const int Quality);

extern BOOL Jpg_ReadControlFromFile(int Panel, int Control, int ImageID,
					const char* FileName);
#endif


///////////////////////////////////////////////////////////////////////////////
extern void Jpg_CopyRGBtoGray(unsigned char *Dest, unsigned char *Source, 
						  const int NbPixels, const int Ratio);
extern void Jpg_CopyRGB32to24(unsigned char *Dest, unsigned char *Source, 
						  const int NbPixels);

#endif // _READ_SAVE_JPG
