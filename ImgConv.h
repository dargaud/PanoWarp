#ifndef _IMG_CONV
#define _IMG_CONV

#include "Def.h"

extern void ConvertRGBToMono( const DWORD * pRGBImage, const DWORD xSize, const DWORD ySize, DWORD ImgSize, 
					DWORD *pMono32[], const DWORD Decimation);

extern void DecimateRGB32( DWORD * pRGBImage, const DWORD xSize, const DWORD ySize, DWORD ImgSize, 
				DWORD *pRGB32, const DWORD Decimation);

extern void DecimateImage( unsigned char *InImage, 
				const int xSize, const int ySize, const int PixDepth, 
				unsigned char *OutImage, const int Decimation);

#endif // _IMG_CONV
