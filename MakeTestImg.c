#include "Def.h"
#include "ColorScale.h"
#include "ReadSavePng.h"

#define STEP

#ifdef STEP
#define ST "Step"
#define S_RGB(a,b,c) ColorStepsRGB(a,b,c,8)
#define S_HSL(a,b,c) ColorStepsHSL(a,b,c,8)
#else
#define ST "Scale"
#define S_RGB(a,b,c) ColorScaleRGB(a,b,c)
#define S_HSL(a,b,c) ColorScaleHSL(a,b,c)
#endif

void TwoColors(void) {
	#define Width 29
	#define Height 256
	int Bits[Width*Height];

	int PixDepth=32, RowBytes=PixDepth*Width/8;
	int Status, y, Col, *Pos=Bits;
	float R;
	
	for (y=0; y<Height; y++) {
		Pos=&Bits[y*Width];
		Col=y*255/(Height-1);
		R=(float)y/(Height-1);
		*Pos++=S_RGB(0, 0xFF0000, R);
		*Pos++=S_RGB(0, 0x00FF00, R);
		*Pos++=S_RGB(0, 0x0000FF, R);
		*Pos++=S_RGB(0, 0x00FFFF, R);
		*Pos++=S_RGB(0, 0xFF00FF, R);
		*Pos++=S_RGB(0, 0xFFFF00, R);
		*Pos++=S_RGB(0, 0xFFFFFF, R);

		*Pos++=S_RGB(0xFFFFFF, 0xFF0000, R);
		*Pos++=S_RGB(0xFFFFFF, 0x00FF00, R);
		*Pos++=S_RGB(0xFFFFFF, 0x0000FF, R);
		*Pos++=S_RGB(0xFFFFFF, 0x00FFFF, R);
		*Pos++=S_RGB(0xFFFFFF, 0xFF00FF, R);
		*Pos++=S_RGB(0xFFFFFF, 0xFFFF00, R);
		*Pos++=S_RGB(0xFFFFFF, 0x000000, R);

		*Pos++=S_RGB(0xFF0000, 0x00FF00, R);
		*Pos++=S_RGB(0xFF0000, 0x0000FF, R);
		*Pos++=S_RGB(0xFF0000, 0x00FFFF, R);
		*Pos++=S_RGB(0xFF0000, 0xFF00FF, R);
		*Pos++=S_RGB(0xFF0000, 0xFFFF00, R);
		*Pos++=S_RGB(0x00FF00, 0x0000FF, R);
		*Pos++=S_RGB(0x00FF00, 0x00FFFF, R);
		*Pos++=S_RGB(0x00FF00, 0xFF00FF, R);
		*Pos++=S_RGB(0x00FF00, 0xFFFF00, R);
		*Pos++=S_RGB(0x0000FF, 0x00FFFF, R);
		*Pos++=S_RGB(0x0000FF, 0xFF00FF, R);
		*Pos++=S_RGB(0x0000FF, 0xFFFF00, R);
		*Pos++=S_RGB(0x00FFFF, 0xFF00FF, R);
		*Pos++=S_RGB(0x00FFFF, 0xFFFF00, R);
		*Pos++=S_RGB(0xFF00FF, 0xFFFF00, R);
	}
	
	Status = Png_SaveBitmapToFile ((unsigned char*)Bits, RowBytes, PixDepth, Width, Height, ST"RGB.png");
	
	for (y=0; y<Height; y++) {
		Pos=&Bits[y*Width];
		Col=y*255/(Height-1);
		R=(float)y/(Height-1);
		*Pos++=S_HSL(0, 0xFF0000, R);
		*Pos++=S_HSL(0, 0x00FF00, R);
		*Pos++=S_HSL(0, 0x0000FF, R);
		*Pos++=S_HSL(0, 0x00FFFF, R);
		*Pos++=S_HSL(0, 0xFF00FF, R);
		*Pos++=S_HSL(0, 0xFFFF00, R);
		*Pos++=S_HSL(0, 0xFFFFFF, R);

		*Pos++=S_HSL(0xFFFFFF, 0xFF0000, R);
		*Pos++=S_HSL(0xFFFFFF, 0x00FF00, R);
		*Pos++=S_HSL(0xFFFFFF, 0x0000FF, R);
		*Pos++=S_HSL(0xFFFFFF, 0x00FFFF, R);
		*Pos++=S_HSL(0xFFFFFF, 0xFF00FF, R);
		*Pos++=S_HSL(0xFFFFFF, 0xFFFF00, R);
		*Pos++=S_HSL(0xFFFFFF, 0x000000, R);

		*Pos++=S_HSL(0xFF0000, 0x00FF00, R);
		*Pos++=S_HSL(0xFF0000, 0x0000FF, R);
		*Pos++=S_HSL(0xFF0000, 0x00FFFF, R);
		*Pos++=S_HSL(0xFF0000, 0xFF00FF, R);
		*Pos++=S_HSL(0xFF0000, 0xFFFF00, R);
		*Pos++=S_HSL(0x00FF00, 0x0000FF, R);
		*Pos++=S_HSL(0x00FF00, 0x00FFFF, R);
		*Pos++=S_HSL(0x00FF00, 0xFF00FF, R);
		*Pos++=S_HSL(0x00FF00, 0xFFFF00, R);
		*Pos++=S_HSL(0x0000FF, 0x00FFFF, R);
		*Pos++=S_HSL(0x0000FF, 0xFF00FF, R);
		*Pos++=S_HSL(0x0000FF, 0xFFFF00, R);
		*Pos++=S_HSL(0x00FFFF, 0xFF00FF, R);
		*Pos++=S_HSL(0x00FFFF, 0xFFFF00, R);
		*Pos++=S_HSL(0xFF00FF, 0xFFFF00, R);
	}
	
	Status = Png_SaveBitmapToFile ((unsigned char*)Bits, RowBytes, PixDepth, Width, Height, ST"HSL.png");
	#undef Width
	#undef Height
}


///////////////////////////////////////////////////////////////////////////////
#define METHOD 5

void ThreeColors(int C1, int C2, int C3, char* Name) {
	#define SIN60  0.86602540378443864676372317075294
	#define ITAN60 0.57735026918962576450914878050196
	#define Width 256
	#define Height (int)(256*SIN60)
	#define Xc (Width/2)	// Center of Triangle
	#define Yc (Height/2)
	#define Dist(x,y,Xp,Yp) sqrt(((x)-(Xp))*((x)-(Xp))+((y)-(Yp))*((y)-(Yp)))
	#define L (Width/(2*SIN60))
	#define H (Width*SIN60)
	
	char FileName[MAX_FILENAME_LEN], Str[255];
	int BitsRGB[Width*Height], BitsHSL[Width*Height];

	int PixDepth=32, RowBytes=PixDepth*Width/8;
	int Status, x, y, Col, r2, r3;
	float R1, R2, R3, Temp, Beta;
//	int C1=0xFF0000, C2=0x00FF00, C3=0x0000FF;
	
	memset (BitsRGB, 0xFF, Width*Height*4);
	memset (BitsHSL, 0xFF, Width*Height*4);

	for (y=0; y<Height; y++)
		for (x=y*ITAN60; x<Width-1-y*ITAN60; x++) {
#if METHOD==1	// indep dist to corner, best method
			R1=1-Dist(x,y, 0,0)/Height;
			R2=1-Dist(x,y, Width/2, Height)/Height;
			R3=1-Dist(x,y, Width-1, 0)/Height;
#elif METHOD==2	// R1�+R2�+R3�=1
			R2=1-Dist(x,y, Width/2, Height)/Height;
			R3=1-Dist(x,y, Width-1, 0)/Height;
			Temp=1-R2*R2-R3*R3;
			R1= (Temp>0) ? sqrt(Temp) : 0;
#elif METHOD==3	// R1+R2+R3=1
			R2=1-Dist(x,y, Width/2, Height)/Height;
			R3=1-Dist(x,y, Width-1, 0)/Height;
			Temp=1-R2-R3;
			R1= (Temp>0) ? (Temp<1 ? Temp : 1) : 0;
#elif METHOD==4	// Distance to center
			R1=1-Dist(x,y, 0,0)/L;
			R2=1-Dist(x,y, Width/2, Height)/L;
			R3=1-Dist(x,y, Width-1, 0)/L;
#elif METHOD==5	// linear dist to corner, best method
			Temp=Dist(x,y, 0,0);
			Beta=atan2(y,x)-DegToRad*30;
			R1=1-Temp*cos(Beta)/H;
			R2=(float)y/Height;
			Temp=Dist(x,y, Width-1,0);
			Beta=atan2(y,Width-1-x)-DegToRad*30.;
			R3=1-Temp*cos(Beta)/H;
#endif			
			if (R1<0) R1=0; else if (R1>1) R1=1;
			if (R2<0) R2=0; else if (R2>1) R2=1;
			if (R3<0) R3=0; else if (R3>1) R3=1;
			BitsRGB[x+(Height-1-y)*Width]= ColorScaleRGB3(C1, C2, C3, R1, R2, R3);
			BitsHSL[x+(Height-1-y)*Width]= ColorScaleHSL3(C1, C2, C3, R1, R2, R3);
		}
	
	sprintf(Str, "RGB scale between %s", Name);
	Png_SetTextTitle (Str, FALSE);
	sprintf(FileName, "ScaleRGB3_%s.png", Name);
	Status = Png_SaveBitmapToFile ((unsigned char*)BitsRGB, RowBytes, PixDepth, Width, Height, FileName);

	sprintf(Str, "HSL scale between %s", Name);
	Png_SetTextTitle (Str, FALSE);
	sprintf(FileName, "ScaleHSL3_%s.png", Name);
	Status = Png_SaveBitmapToFile ((unsigned char*)BitsHSL, RowBytes, PixDepth, Width, Height, FileName);

	
	#undef Width
	#undef Height
}



///////////////////////////////////////////////////////////////////////////////
int main (int argc, char *argv[]) {
	Png_SetParameters (1, 0, 28, Z_BEST_COMPRESSION, 0);
	Png_SetTextAuthor ("Guillaume Dargaud", FALSE);
	Png_SetTextCopyright ("(c) 2001-2002 Guillaume Dargaud", FALSE);
	Png_SetTextSoftware("http://www.gdargaud.net/Hack/GenPalette.html", FALSE);

	TwoColors();

#if 0
	#define R 0xFF0000
	#define G 0x00FF00
	#define B 0x0000FF
	#define C 0x00FFFF
	#define M 0xFF00FF
	#define Y 0xFFFF00
	#define W 0xFFFFFF
	#define K 0x000000
	#define Generate(C1, C2, C3) ThreeColors(C1, C2, C3, #C1#C2#C3)

	Generate(R, G, B);
	Generate(R, G, C);
	Generate(R, G, M);
	Generate(R, G, Y);
	Generate(R, G, W);
	Generate(R, G, K);
	Generate(R, B, C);
	Generate(R, B, M);
	Generate(R, B, Y);
	Generate(R, B, W);
	Generate(R, B, K);
	Generate(R, C, M);
	Generate(R, C, Y);
	Generate(R, C, W);
	Generate(R, C, K);
	Generate(R, M, Y);
	Generate(R, M, W);
	Generate(R, M, K);
	Generate(R, Y, W);
	Generate(R, Y, K);
	Generate(R, W, K);

	Generate(G, B, C);
	Generate(G, B, M);
	Generate(G, B, Y);
	Generate(G, B, W);
	Generate(G, B, K);
	Generate(G, C, M);
	Generate(G, C, Y);
	Generate(G, C, W);
	Generate(G, C, K);
	Generate(G, M, Y);
	Generate(G, M, W);
	Generate(G, M, K);
	Generate(G, Y, W);
	Generate(G, Y, K);
	Generate(G, W, K);

	Generate(B, C, M);
	Generate(B, C, Y);
	Generate(B, C, W);
	Generate(B, C, K);
	Generate(B, M, Y);
	Generate(B, M, W);
	Generate(B, M, K);
	Generate(B, Y, W);
	Generate(B, Y, K);
	Generate(B, W, K);

	Generate(C, M, Y);
	Generate(C, M, W);
	Generate(C, M, K);
	Generate(C, Y, W);
	Generate(C, Y, K);
	Generate(C, W, K);

	Generate(M, Y, W);
	Generate(M, Y, K);
	Generate(M, W, K);

	Generate(Y, W, K);
#endif

	return 0;
}
