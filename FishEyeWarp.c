#include <windows.h>
#include <ansi_c.h>
#include <utility.h>
#include <cvirte.h>		
#include <userint.h>

#include "Def.h"
#include "ReadSavePng.h"
#include "ReadSaveJpeg.h"

#include "PanoWarp.h"

#define _VER_ "1.5"			// Version
#define _WEB_ "http://www.gdargaud.net/Hack/PanoWarp.html"
#define _COPY_ "� 2005-2006 Guillaume Dargaud (freeware)"

static char *ShortInfo="FishEyeWarp v" _VER_ " - " _COPY_ " " _WEB_;

static char *Info="FishEyeWarp v" _VER_ " - " _COPY_ " - Last compiled on " __DATE__ "\n\n"
			"This program transforms images taken with a fisheye lens\n"
			"so you can then assemble them into a panorama\n"
			"or just display them without the ugly image circle\n\n"
			"The image MUST have the image circle at least partly visible (using a 15mm lens won't work)\n"
			"You just adjust the overlaid circle to the image circle by moving its 3 border points\n"
			"(Alternately you can move the center and select the radius manually)\n"
			"Then press [Deform] and [Save]. That's it for this prog.\n"
			"Then import the resulting images into your favorite graphic application and assemble them.\n"
			"For better results you should:\n"
			"- have your camera on a perfectly horizontal tripod\n"
			"- rotate the camera around its optical center (avoids parallax)\n"
			"- use manual exposure and fixed white balance for image uniformity\n"
			"- take at least 3 images (the more the better)\n"
			"- when warping a bunch of images, the radius should stay the same (change only the center)\n\n"
			"Right-click on any control for further help\n"
			"Tutorial at " _WEB_;
   						
static int Pnl;
static int BmpIn=0, BmpOut=0;
static unsigned char *BitsIn=NULL, *BitsOut=NULL;
static int RowBytes, Height, Width, PixDepth;	// Input image
static int RowBytesOut, HeightOut, WidthOut, PixDepthOut;	// Output image
static int cWidth, cHeight, cTop, cLeft;		// Canvas size

static double X0=-1, Y0=-1, X1=-1, Y1=-1, X2=-1, Y2=-1;
static int ColorRef=0xFF4444, ColorCircle=0x4444FF, ColorRotation=0xF8BA6A;
static double Xc=-1, Yc=-1, Radius=100;	// Center and radius of the image circle
static double Rotation=0;	// For the rotation angle
static double ZoomX=1, ZoomY=1;
static const double ArbitraryAngle0=DegToRad*30, 	// Default position of the border points
					ArbitraryAngle1=DegToRad*(180-30), 
					ArbitraryAngle2=DegToRad*(180+30);

static BOOL T=FALSE,	// Whether the image is transformed or not
			Vertical=FALSE,	// If the camera was pointing vertically or horizontally
			CanvasFocused=FALSE;	// If the focus is on the canvas or not

static char Drive[MAX_DRIVENAME_LEN], Dir[MAX_DIRNAME_LEN], FileName[MAX_PATHNAME_LEN];

static BOOL LoadFile(void);
static void DrawCircle(void);


///////////////////////////////////////////////////////////////////////////////
int main (int argc, char *argv[]) {
	if (InitCVIRTE (0, argv, 0) == 0)
		return -1;	/* out of memory */
	if ((Pnl = LoadPanel (0, "PanoWarp.uir", FE)) < 0)
		return -1;

	DisplayPanel (Pnl);
	SetCtrlVal(Pnl, FE_TEXTMSG, ShortInfo);
	cbp_Panel (Pnl, EVENT_PANEL_SIZE, NULL, 0, 0);
	#ifndef _CVI_DEBUG_
	cbp_Panel (Pnl, EVENT_RIGHT_CLICK, NULL, 0, 0);
	#endif
	
	// catch dragged-and-dropped file
	EnableDragAndDrop (Pnl);
	
	// Various tries to make a better UIR
	
	// Use a specific callback to catch mouse-down
//	InstallWinMsgCallback (Pnl, WM_LBUTTONDOWN, cbwin_Canvas,
//						   VAL_MODE_IN_QUEUE, NULL, HWND)
	// Catch win messages directly into the panel callback
	RouteWinMsgToPanelCallback (Pnl, WM_LBUTTONDOWN);
	RouteWinMsgToPanelCallback (Pnl, WM_LBUTTONUP);
	
	// Catch every mouse event
//	SetWindowsHookEx()
	
	
	///////////////////
	RunUserInterface();
	///////////////////
	
	DisableDragAndDrop (Pnl);
	DiscardPanel (Pnl);
	return 0;
}


///////////////////////////////////////////////////////////////////////////////
// Set title of the panel with filename and zoom factor
void SetTitle(void) {
	char Title[MAX_PATHNAME_LEN+100];
	if (FileName==NULL or FileName[0]=='\0') {
		sprintf(Title, "Zoom %.0f - Fisheye warping", ZoomX);
	} else {
		SplitPath (FileName, Drive, Dir, Title);
		memmove (Dir+strlen(Drive), Dir, strlen(Dir)+1);
		memmove (Dir, Drive, strlen(Drive));		// We now have the path inside Dir
		sprintf(Title, "%s - Zoom 1:%.0f - Fisheye warping", Title, ZoomX);
	}	
	SetPanelAttribute (Pnl, ATTR_TITLE, Title);
}


///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cbp_Panel (int panel, int event, void *callbackData,
		int eventData1, int eventData2) {
	typedef struct {
		unsigned int uMsg; // Windows message code
		unsigned int wParam;
		long lParam;
		long result;
	} InterceptedWindowsMsg;
	InterceptedWindowsMsg *msg;
	int X, Y, Left, Top;
	int pWidth, pHeight, sWidth, sHeight, i;
	
	switch (event) {
		case EVENT_PANEL_SIZE:
			GetPanelAttribute(Pnl, ATTR_HEIGHT, &pHeight);
			GetPanelAttribute(Pnl, ATTR_WIDTH, &pWidth);
//			GetScreenSize (&sHeight, &sWidth);
//			pHeight=Min(pHeight,sHeight);
//			pWidth=Min(pWidth,sWidth);
			
			GetCtrlAttribute(Pnl, FE_CANVAS, ATTR_LEFT,&cLeft);
			GetCtrlAttribute(Pnl, FE_CANVAS, ATTR_TOP, &cTop);
			SetCtrlAttribute(Pnl, FE_TRANSPARENT, ATTR_LEFT,cLeft);
			SetCtrlAttribute(Pnl, FE_TRANSPARENT, ATTR_TOP, cTop);
//			if (pHeight<120) SetPanelAttribute(Pnl, ATTR_HEIGHT, pHeight=120);
			if (Width==0 or Height==0) { Width=400; Height=300; }
			#define CurrentImageWidth (T?WidthOut:Width)
			#define CurrentImageHeight (T?HeightOut:Height)
			cWidth=pWidth-cLeft;
			cHeight=pHeight-cTop;
			ZoomX=(double)CurrentImageWidth/cWidth;
			ZoomY=(double)CurrentImageHeight/cHeight;
			ZoomX=ZoomY=Max(ZoomX, ZoomY);	
			SetCtrlAttribute(Pnl, FE_CANVAS, ATTR_WIDTH,  cWidth=RoundRealToNearestInteger(CurrentImageWidth/ZoomX));
			SetCtrlAttribute(Pnl, FE_CANVAS, ATTR_HEIGHT, cHeight=RoundRealToNearestInteger(CurrentImageHeight/ZoomY));
			SetCtrlAttribute(Pnl, FE_TRANSPARENT, ATTR_WIDTH,  cWidth);
			SetCtrlAttribute(Pnl, FE_TRANSPARENT, ATTR_HEIGHT, cHeight);
			SetPanelSize(Pnl, pHeight=cHeight+cTop, pWidth=cWidth+cLeft);
			SetTitle();
			if ((T ? BmpOut : BmpIn)!=0) 
				CanvasDrawBitmap (Pnl, FE_CANVAS, T ? BmpOut : BmpIn, VAL_ENTIRE_OBJECT, VAL_ENTIRE_OBJECT);
			break;
		
		case EVENT_FILESDROPPED:
			i=0;
			while (((char**)eventData1)[i]!=NULL and ((char**)eventData1)[i][0]!='\0') {
				strcpy(FileName, ((char**)eventData1)[i]);
				if (!LoadFile()) return 1;
				i++;
			}
			break;
		
		case EVENT_WINDOWS_MSG:
			msg = (InterceptedWindowsMsg *)eventData1;
			if (/*msg->wParam and*/ /*CanvasFocused and*/ 
				( msg->uMsg==WM_LBUTTONDOWN or msg->uMsg==WM_LBUTTONUP ) ) {
				GetGlobalMouseState (NULL, &X, &Y, NULL, NULL, NULL);
				GetPanelAttribute (Pnl, ATTR_LEFT, &Left);
				GetPanelAttribute (Pnl, ATTR_TOP, &Top);
				// send custom mouse-down or mouse-up event
				cb_Canvas (Pnl, FE_TRANSPARENT, msg->uMsg, NULL, Y-Top, X-Left);
    		}
			break;
		
		case EVENT_RIGHT_CLICK:
   			MessagePopup("FishEyeWarp Help", Info);
			return 1;
	
		case EVENT_KEYPRESS:
			if (eventData1==VAL_F1_VKEY) {
	   			MessagePopup("FishEyeWarp Help", Info);
				return 1;
			}
	}
	return 0;
}



///////////////////////////////////////////////////////////////////////////////
// Set the size respecting the ratio, if it fits
// TODO: size for Out
void SetCanvasSize(void) {
	int ScrHeight, ScrWidth, Left, Top, pLeft, pTop;
	int Zoom;
	GetScreenSize (&ScrHeight, &ScrWidth);
	GetPanelAttribute(Pnl, ATTR_LEFT, &pLeft);
	GetPanelAttribute(Pnl, ATTR_TOP, &pTop);
	
	ZoomX=ZoomY=Zoom=Max(1, 1+sqrt(CurrentImageHeight*CurrentImageHeight+CurrentImageWidth*CurrentImageWidth)/
						sqrt((ScrHeight-pTop)*(ScrHeight-pTop)+(ScrWidth-pLeft)*(ScrWidth-pLeft)) );

	SetCtrlAttribute(Pnl, FE_CANVAS, ATTR_WIDTH,  cWidth=CurrentImageWidth/ZoomX);
	SetCtrlAttribute(Pnl, FE_CANVAS, ATTR_HEIGHT, cHeight=CurrentImageHeight/ZoomY);
	SetCtrlAttribute(Pnl, FE_TRANSPARENT, ATTR_WIDTH,  cWidth);
	SetCtrlAttribute(Pnl, FE_TRANSPARENT, ATTR_HEIGHT, cHeight);
	GetCtrlAttribute(Pnl, FE_CANVAS, ATTR_LEFT,  &Left);
	GetCtrlAttribute(Pnl, FE_CANVAS, ATTR_TOP, &Top);
	SetPanelAttribute(Pnl, ATTR_WIDTH, CurrentImageWidth/Zoom+Left);
	SetPanelAttribute(Pnl, ATTR_HEIGHT, CurrentImageHeight/Zoom+Top);
	SetTitle();
	if ((T ? BmpOut : BmpIn)!=0) 
		CanvasDrawBitmap (Pnl, FE_CANVAS, T ? BmpOut : BmpIn, VAL_ENTIRE_OBJECT, VAL_ENTIRE_OBJECT);
}


///////////////////////////////////////////////////////////////////////////////
// Various UIR changes depending if image is original or transformed
void Transformed(void) {
	SetCanvasSize();
	SetCtrlAttribute(Pnl, FE_COPY, ATTR_DIMMED, !T);
	SetCtrlAttribute(Pnl, FE_SAVE, ATTR_DIMMED, !T);
	SetCtrlAttribute(Pnl, FE_DEFORM, ATTR_DIMMED, FALSE);
	SetCtrlAttribute(Pnl, FE_RADIUS, ATTR_DIMMED, T);
	SetCtrlAttribute(Pnl, FE_ROTATION, ATTR_DIMMED, T);
	SetCtrlAttribute(Pnl, FE_CENTER, ATTR_DIMMED, T);
	SetCtrlAttribute(Pnl, FE_VERTICAL, ATTR_DIMMED, T);
	SetCtrlVal(Pnl, FE_DEFORM, T);
	if ((T ? BmpOut : BmpIn)!=0) 
		CanvasDrawBitmap (Pnl, FE_CANVAS, T ? BmpOut : BmpIn, VAL_ENTIRE_OBJECT, VAL_ENTIRE_OBJECT);
	SetCtrlAttribute(Pnl, FE_TRANSPARENT, ATTR_VISIBLE, !T);
}


///////////////////////////////////////////////////////////////////////////////
// Return TRUE if file not readable
static BOOL LoadFile(void) {
	int Status, PreviousHeight, PreviousWidth;
	PreviousHeight=Height; PreviousWidth=Width;
	SetWaitCursor(1);
	SetTitle();
	
	if (BmpIn!=0) DiscardBitmap (BmpIn); BmpIn=0;

	if (toupper(FileName[strlen(FileName)-3])=='P' and
		toupper(FileName[strlen(FileName)-2])=='N' and
		toupper(FileName[strlen(FileName)-1])=='G') 
		Status = Png_ReadBitmapFromFile (&BmpIn, &BitsIn, &RowBytes, &PixDepth, &Width, &Height, FileName);
	else
	if (toupper(FileName[strlen(FileName)-3])=='J' and
		toupper(FileName[strlen(FileName)-2])=='P' and
		toupper(FileName[strlen(FileName)-1])=='G' or
		toupper(FileName[strlen(FileName)-4])=='J' and
		toupper(FileName[strlen(FileName)-3])=='P' and
		toupper(FileName[strlen(FileName)-2])=='E' and
		toupper(FileName[strlen(FileName)-1])=='G') 
		Status = Jpg_ReadBitmapFromFile (&BmpIn, &BitsIn, &RowBytes, &PixDepth, &Width, &Height, FileName);
	else {
		MessagePopup("Error", "Selected file is not readable");
		return TRUE;
	}
	
	SetWaitCursor(0);
	if (Status) MessagePopup("Error", "Selected file is not readable");
//			else SetCanvasSize();
	
	if (PreviousHeight!=Height or PreviousWidth!=Width) {
		Xc=Width/2; Yc=Height/2;
		X0=X1=X2=-1;
	}
	if (Radius==100) SetCtrlVal(Pnl, FE_RADIUS, Radius=Min(Xc*ZoomX, Yc*ZoomY));
	SetCanvasSize();
		
	T=FALSE;
	Transformed();
	DrawCircle();
	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_Load (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	char Title[MAX_PATHNAME_LEN+100];
	int Status;
	
	switch (event) {
		case EVENT_COMMIT:
			Status = FileSelectPopup ("", "*.png;*.jpg;*.jpeg", "*.png;*.jpg;*.jpeg",
									  "Open png/jpg file", VAL_LOAD_BUTTON,
									  0, 0, 1, 1, FileName);
			if (Status!=VAL_EXISTING_FILE_SELECTED) return 0;
			LoadFile();			
			break;
		case EVENT_RIGHT_CLICK:
   			MessagePopup("Load", "Selects and loads a PNG or JPG image file.");
			return 1;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_Save (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	char Title[MAX_PATHNAME_LEN+100], Response[5]="75";
	static BOOL FirstCallPng=TRUE, FirstCallJpg=TRUE;
	static int CompressionJpeg=75, Status;
	time_t Time=time(NULL);
	char *Dot, Sample[MAX_PATHNAME_LEN];

	switch (event) {
		case EVENT_COMMIT:
			if (strlen(FileName)==0) strcpy(FileName,"FisheyeToPano.png");
			Dot=strrchr (FileName, '.');
			strncpy(Sample, FileName, Dot-FileName);
			Sample[Dot-FileName]='\0';
			strcat(Sample, "W.png");
			
			Status = FileSelectPopup (Dir, Sample, "*.png;*.jpg;*.jpeg",
									  "Save warped image to ?", VAL_SAVE_BUTTON,
									  0, 0, 1, 1, FileName);
			if (Status!=VAL_EXISTING_FILE_SELECTED and Status!=VAL_NEW_FILE_SELECTED) return 0;
			SetWaitCursor(1);
			
			if (toupper(FileName[strlen(FileName)-3])=='P' and
				toupper(FileName[strlen(FileName)-2])=='N' and
				toupper(FileName[strlen(FileName)-1])=='G') {
				if (FirstCallPng) {
					Png_SetParameters (0.45455, 0, 28, Z_DEFAULT_COMPRESSION, 0);
					Png_SetTextTitle("Panorama 360x90�", FALSE);
					Png_SetTextDescription("Imaged warped from original fisheye image into a 360x90� panorama", FALSE);
					// Png_SetTextCopyright("Image � ??? - Warping software " _COPY_, FALSE);
					Png_SetTextCreationTime(ctime(&Time), FALSE);
					Png_SetTextSoftware(ShortInfo, FALSE);
					Png_SetTextComment("This is a fake rectilinear (or true spherical) projection converted from a fisheye image.\n"
										"It can be used as an intermediate picture in the creation of a 360deg panorama", FALSE);
					FirstCallPng=FALSE;
				}
				Png_SaveBitmapToFile (BitsOut, RowBytesOut, PixDepth, WidthOut, HeightOut, FileName);
			}
			else
			if ((toupper(FileName[strlen(FileName)-3])=='J' and
				 toupper(FileName[strlen(FileName)-2])=='P' and
				 toupper(FileName[strlen(FileName)-1])=='G') or 
				(toupper(FileName[strlen(FileName)-4])=='J' and
				 toupper(FileName[strlen(FileName)-3])=='P' and
				 toupper(FileName[strlen(FileName)-2])=='E' and
				 toupper(FileName[strlen(FileName)-1])=='G') ) {
				if (FirstCallJpg) {
					PromptPopup ("JPEG compression quality ?",
								 "Please choose the compression ratio for the JPEG compression.\n"
								 "1=high compression, poor quality\n"
								 "100=no compression, highest quality\n"
								 "75=default compression, good quality",
								 Response, 4);
					CompressionJpeg=atoi(Response);
					if (strlen(Response)==0) CompressionJpeg=75;
					if (CompressionJpeg<1) CompressionJpeg=75;
					if (CompressionJpeg>100) CompressionJpeg=75;
					FirstCallJpg=FALSE;
				}
				Jpg_SaveBitmapToFile (BitsOut, RowBytesOut, PixDepth, WidthOut, HeightOut, FileName, CompressionJpeg);
			} 
			else MessagePopup("Error!", "Error, invalid file extension specified.");

			SetWaitCursor (0);
			break;
		case EVENT_RIGHT_CLICK:
   			MessagePopup("Load", "Save warped image to a compressed png or jpg file.");
			return 1;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_PasteFromClipboad (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	static BOOL FirstCall=TRUE;
	int Error, Status, *ColorTable, PreviousHeight, PreviousWidth;
	unsigned char *Mask;
	BOOL Available;
	
	switch (event) {
		case EVENT_COMMIT:
			if (FirstCall)
				MessagePopup("Warning !",
							"A unidentified bug in the paste function looses some colors when pasting."
							"\nYou can use it, but the result won't look as good as if you read from a file."
							"\nAlso the size of the images you can paste is somewhat limited.");
			FirstCall=FALSE;
			SetWaitCursor (1); // Turn on wait cursor

			strcpy(FileName, ""); SetTitle();
			if (BmpIn!=0) DiscardBitmap (BmpIn); BmpIn=0;
			Error=ClipboardGetBitmap (&BmpIn, &Available);
			
			SetWaitCursor (0);
			if (Error!=0) {
				MessagePopup ("Error !", GetUILErrorString(Error));
				return 0;
			} else if (!Available) {
				MessagePopup ("Warning !", "The clipboard does not seem to contain a valid image.");
				return 0;
			}

			Status = AllocBitmapData (BmpIn, &ColorTable, &BitsIn, &Mask);
			if (Status!=0) { MessagePopup("Error!",GetUILErrorString (Status)); /*QuitUserInterface(1);*/ }
			if (ColorTable!=NULL) {	
				MessagePopup ("Warning !", "This program operates only on\n16 million color files.\nExiting.");
				return 0; 
			}
			if (Mask!=NULL) {	
				MessagePopup ("Warning !", "This program operates only on\nnon masked files.\nExiting.");
				return 0; 
			}
	
			PreviousHeight=Height; PreviousWidth=Width;
			Status = GetBitmapData (BmpIn, &RowBytes, &PixDepth, &Width, &Height, ColorTable, BitsIn, Mask);
			
			if (PreviousHeight!=Height or PreviousWidth!=Width) {
				Xc=Width/2; Yc=Height/2;
				X0=X1=X2=-1;
			}
			if (Radius==100) SetCtrlVal(Pnl, FE_RADIUS, Radius=Min(Xc*ZoomX, Yc*ZoomY));
			SetCanvasSize();

			T=FALSE;
			Transformed();
			DrawCircle();
			
			break;
		
		case EVENT_RIGHT_CLICK:
   			MessagePopup("Paste", "Paste a normal picture from the clipboard\n"
   								"(16-bit images or paletted images not accepted).\n"
   								"Keyboard shortcut: [Ctrl][V]");
			return 1;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_CopyToClipboad (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int Status;
	switch (event) {
		case EVENT_COMMIT:
			SetWaitCursor(1);
			Status=ClipboardPutBitmap (BmpOut);
			if (Status!=0) { MessagePopup("Error!",GetUILErrorString (Status)); }
			SetWaitCursor(0);
			break;
		case EVENT_RIGHT_CLICK:
   			MessagePopup("Copy", "Copy the deformed picture to the clipboard.[Ctrl][C] shortcut.");
			return 1;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_Quit (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			QuitUserInterface (0);
			break;
		case EVENT_RIGHT_CLICK:
   			MessagePopup("Help Quit", "What do you seriously expect to see here ?!?");
			return 1;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
#define METHOD_SQUARE 0		// Produces square image, horizontal stretch
#define METHOD_LINEAR 1		// Not good
#define METHOD_BACKCIRCLE 2	// Produces inverted circle (not good)
#define METHOD_HYPERBOLIC 3		// Hyperbolic cosine (not good)
#define METHOD_FULLSQUARE 4		// Produces square image, central stretch (not good)

#define Method METHOD_SQUARE	// A good method will produce vertical lines that _are_ vertical
								// The original verticals are elipses on the fisheye image

int CVICALLBACK cb_Deform (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	unsigned char *Source, *Dest;
	int Progress;
	int x, y, Xs, Ys, Xd, Xw;
	double Yr, r, Rd, cAlpha, sAlpha, tAlpha;
	double cR, sR;
	cR=cos(Rotation); sR=sin(Rotation);

	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(Pnl, FE_DEFORM, &T);
			if (T) {
				SetWaitCursor(TRUE);
				if (Vertical) { HeightOut=Radius; WidthOut=4/*2*PI*/*Radius; }	// 4 to keep proportion 360/90, 2Pi to keep perimeter length
				else          { HeightOut=Height; WidthOut=2*Radius;         } 
				RowBytesOut=WidthOut*PixDepth/8;
				if (BmpOut!=0) DiscardBitmap (BmpOut); BmpOut=0;
				BitsOut=realloc(BitsOut, HeightOut*RowBytesOut*PixDepth/8);
				memset (BitsOut, 0, HeightOut*RowBytesOut*PixDepth/8);
				
				Progress = CreateProgressDialog ("Warping", "Percent Complete", 1, VAL_FULL_MARKERS, "Cancel");
				
				if (Vertical) 		// The camera was vertical
					for (x=0; x<WidthOut; x++) {
						if (UpdateProgressDialog (Progress, 100*x/WidthOut, 0)) {
							T=FALSE; Transformed(); return 0; }		// Manual abort
						cAlpha=cos(Rotation-2*Pi*x/WidthOut);
						sAlpha=sin(Rotation-2*Pi*x/WidthOut);
						for (y=0; y<HeightOut; y++) {	// Yr is centered around the center
							Xs=Xc+y*cAlpha;				// Source pixel
							Ys=Yc+y*sAlpha;
							if (!BETWEEN(0,Xs,Width-1) or !BETWEEN(0,Ys,Height-1) ) 
								continue;
							Source=&BitsIn[Xs*PixDepth/8+Ys*RowBytes];
							Dest=&BitsOut[x*PixDepth/8+y*RowBytesOut];
							*Dest=*Source;
							if (PixDepth>8) {
								*++Dest=*++Source;
								*++Dest=*++Source;
								if (PixDepth==32)
									*++Dest=*++Source;
							}
						}
					}
				else				// The camera was horizontal
					for (Yr=-(Radius-1); Yr<=Radius-1; Yr++) {	// Yr is centered around the center
						if (UpdateProgressDialog (Progress, 50*(Yr+Radius)/Radius, 0)) {
							T=FALSE; Transformed(); return 0; }		// Manual abort

						for (Xd=0; Xd<WidthOut; Xd++) {
							Xw=Xd-WidthOut/2;
							r=sqrt(Radius*Radius-Yr*Yr);	// r is the horizontal radius of the circle
															// The source point is somewhere along (-r/+r, Ys)
							switch (Method) {			// This is the destination elongation
								// When r=Radius (Y=0), Rd should be equal to Radius.
								// There should also be a vertical symetry
								case METHOD_SQUARE:		Rd=Radius; 			break;
								case METHOD_LINEAR:		Rd=fabs(Yr)+Radius;	break;
								case METHOD_BACKCIRCLE:	Rd=2*Radius-r;		break;
								case METHOD_HYPERBOLIC:	Rd=0.5*Radius*Radius*(1/(Radius-Yr)+1/(Radius+Yr)); break;
								case METHOD_FULLSQUARE:	/* later */ break;
							}
							
							if (Method==METHOD_FULLSQUARE) {
								Rd=MAX(fabs(Xw),fabs(Yr))/sqrt(Xw*Xw+Yr*Yr);
								Xs=Xc+(Xw*cR-Yr*sR)*Rd;		// Central homothetie + Rotational matrix
								Ys=Yc+(Xw*sR+Yr*cR)*Rd;
							} else {								// Horizontal stretches
	//							Xs=Xc+Xw*r/Rd;				// Xc is the center of the circle
								Xs=Xc+Xw*cR*r/Rd-Yr*sR;	// Rotational matrix + horizontal deform
								Ys=Yc+Xw*sR*r/Rd+Yr*cR;
							}
							
							if (BETWEEN(0,Xs,Width-1) and BETWEEN(0, Ys, Height-1))
								Source=&BitsIn[Xs*PixDepth/8+Ys*RowBytes];
							else Source=(unsigned char*)"\0\0\0\0";		// Point to zeros
//							Dest=&BitsOut[Xd*PixDepth/8+Ys*RowBytesOut];
							if (!BETWEEN(0,Yc+Yr,HeightOut)) continue;
							Dest=&BitsOut[Xd*PixDepth/8+(int)(Yc+Yr)*RowBytesOut];
							*Dest=*Source;
							if (PixDepth>8) {
								*++Dest=*++Source;
								*++Dest=*++Source;
								if (PixDepth==32)
									*++Dest=*++Source;
							}
						}
					}
				DiscardProgressDialog (Progress);
				if (0>NewBitmap (RowBytesOut, PixDepth, WidthOut, HeightOut, NULL, BitsOut, NULL, &BmpOut)) {
					MessagePopup("Error", "Not enough memory to display the image,\nbut you can probably still try to save it.");
				}
			}
			Transformed();
			SetWaitCursor(FALSE);
			break;
		
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help Deform", "Deform the original fisheye image,\n"
						"using the circle you drew,\n"
						"into a square image which can be used to assemble a panorama manually");
			return 1;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// Position circle
static void DrawCircle(void) {
//	double R2;
	CanvasClear (Pnl, FE_TRANSPARENT, VAL_ENTIRE_OBJECT);
	SetCtrlAttribute (Pnl, FE_TRANSPARENT, ATTR_PEN_MODE, VAL_COPY_MODE);

	if (Xc!=-1 /*CircleX[0]!=-1 and CircleX[1]!=-1 and CircleX[2]!=-1*/) {
		SetCtrlVal(Pnl, FE_RADIUS, Radius);
		SetCtrlVal(Pnl, FE_ROTATION, Rotation*RadToDeg);
		SetCtrlAttribute(Pnl, FE_TRANSPARENT, ATTR_PEN_COLOR, ColorCircle);
		SetCtrlAttribute(Pnl, FE_TRANSPARENT, ATTR_PEN_FILL_COLOR, ColorCircle);
		SetCtrlAttribute(Pnl, FE_TRANSPARENT, ATTR_PEN_WIDTH, 5);
		CanvasDrawPoint (Pnl, FE_TRANSPARENT, MakePoint(Xc/ZoomX, Yc/ZoomY));
		
		// Draw Horizontal and vertical line
		SetCtrlAttribute (Pnl, FE_TRANSPARENT, ATTR_PEN_WIDTH, 1);
		// Computes a coordinate centered on Xc/Yc
		#define MakePointPolar(Dist,Angle) MakePoint((Xc+Dist*cos(Angle))/ZoomX, (Yc+Dist*sin(Angle))/ZoomY)
//		CanvasDrawLine (Pnl, FE_TRANSPARENT, MakePoint((Xc-Radius)/ZoomX, Yc/ZoomY), MakePoint((Xc+Radius)/ZoomX, Yc/ZoomY));
//		CanvasDrawLine (Pnl, FE_TRANSPARENT, MakePoint(Xc/ZoomX, (Yc-Radius)/ZoomY), MakePoint(Xc/ZoomX, (Yc+Radius)/ZoomY));
		CanvasDrawLine (Pnl, FE_TRANSPARENT, MakePointPolar(Radius, Rotation-180*DegToRad), MakePointPolar(Radius, Rotation));
		CanvasDrawLine (Pnl, FE_TRANSPARENT, MakePointPolar(Radius, Rotation-90*DegToRad), MakePointPolar(Radius, Rotation+90*DegToRad));
		
		// Draw main circle
		SetCtrlAttribute (Pnl, FE_TRANSPARENT, ATTR_PEN_WIDTH, 2);
		CanvasDrawOval(Pnl, FE_TRANSPARENT, 
			MakeRect((Yc-Radius)/ZoomY, (Xc-Radius)/ZoomX, 2*Radius/ZoomY, 2*Radius/ZoomX), VAL_DRAW_FRAME);
		
		if (Vertical) {
			SetCtrlAttribute (Pnl, FE_TRANSPARENT, ATTR_PEN_WIDTH, 1);
//			R2=Radius/sqrt(2);
//			CanvasDrawLine (Pnl, FE_TRANSPARENT, MakePoint((Xc-R2)/ZoomX, (Yc-R2)/ZoomY), MakePoint((Xc+R2)/ZoomX, (Yc+R2)/ZoomY));
//			CanvasDrawLine (Pnl, FE_TRANSPARENT, MakePoint((Xc-R2)/ZoomX, (Yc+R2)/ZoomY), MakePoint((Xc+R2)/ZoomX, (Yc-R2)/ZoomY));
			CanvasDrawLine (Pnl, FE_TRANSPARENT, MakePointPolar(Radius, Rotation+225*DegToRad), MakePointPolar(Radius, Rotation+45*DegToRad));
			CanvasDrawLine (Pnl, FE_TRANSPARENT, MakePointPolar(Radius, Rotation-225*DegToRad), MakePointPolar(Radius, Rotation-45*DegToRad));
		} else if (Rotation==0) {
			// Draw secondary ellipses
			SetCtrlAttribute (Pnl, FE_TRANSPARENT, ATTR_PEN_WIDTH, 1);
			CanvasDrawOval(Pnl, FE_TRANSPARENT, 
				MakeRect((Yc-Radius)/ZoomY, (Xc-Radius/2)/ZoomX, 2*Radius/ZoomY, Radius/ZoomX), VAL_DRAW_FRAME);
			CanvasDrawOval(Pnl, FE_TRANSPARENT, 
				MakeRect((Yc-Radius)/ZoomY, (Xc-3*Radius/4)/ZoomX, 2*Radius/ZoomY, 3*Radius/2/ZoomX), VAL_DRAW_FRAME);
			CanvasDrawOval(Pnl, FE_TRANSPARENT, 
				MakeRect((Yc-Radius)/ZoomY, (Xc-Radius/4)/ZoomX, 2*Radius/ZoomY, Radius/2/ZoomX), VAL_DRAW_FRAME);
		}
	}
	
	SetCtrlAttribute(Pnl, FE_TRANSPARENT, ATTR_PEN_COLOR, ColorRef);
	SetCtrlAttribute(Pnl, FE_TRANSPARENT, ATTR_PEN_FILL_COLOR, ColorRef);
	SetCtrlAttribute(Pnl, FE_TRANSPARENT, ATTR_PEN_WIDTH, 5);
	
	// Make arbitrary positionned points
	if (X0==-1) { X0=Xc+Radius*cos(ArbitraryAngle0); Y0=Yc+Radius*sin(ArbitraryAngle0); }
	if (X1==-1) { X1=Xc+Radius*cos(ArbitraryAngle1); Y1=Yc+Radius*sin(ArbitraryAngle1); }
	if (X2==-1) { X2=Xc+Radius*cos(ArbitraryAngle2); Y2=Yc+Radius*sin(ArbitraryAngle2); }
	// TODO: test if they are within window
	
	// Plot corner points
	CanvasDrawPoint(Pnl, FE_TRANSPARENT, MakePoint(X0/ZoomX, Y0/ZoomY));
	CanvasDrawPoint(Pnl, FE_TRANSPARENT, MakePoint(X1/ZoomX, Y1/ZoomY));
	CanvasDrawPoint(Pnl, FE_TRANSPARENT, MakePoint(X2/ZoomX, Y2/ZoomY));
	
	// Plot rotation reference
	SetCtrlAttribute(Pnl, FE_TRANSPARENT, ATTR_PEN_COLOR, ColorRotation);
	SetCtrlAttribute(Pnl, FE_TRANSPARENT, ATTR_PEN_FILL_COLOR, ColorRotation);
	SetCtrlAttribute(Pnl, FE_TRANSPARENT, ATTR_PEN_WIDTH, 1);
	CanvasDrawLine  (Pnl, FE_TRANSPARENT, MakePointPolar(0, 0), MakePointPolar(Radius*0.75, Rotation));
	SetCtrlAttribute(Pnl, FE_TRANSPARENT, ATTR_PEN_WIDTH, 5);
	CanvasDrawPoint (Pnl, FE_TRANSPARENT, MakePoint((Xc+Radius*cos(Rotation)*0.75)/ZoomX, (Yc+Radius*sin(Rotation)*0.75)/ZoomY));

	// Cross at center of window for reference
	SetCtrlAttribute (Pnl, FE_TRANSPARENT, ATTR_PEN_WIDTH, 1);
	CanvasDrawLine(Pnl, FE_TRANSPARENT, MakePoint(Width/(2*ZoomX)-5, Height/(2*ZoomY)  ), MakePoint(Width/(2*ZoomX)+5, Height/(2*ZoomY)  ));
	CanvasDrawLine(Pnl, FE_TRANSPARENT, MakePoint(Width/(2*ZoomX)  , Height/(2*ZoomY)-5), MakePoint(Width/(2*ZoomX)  , Height/(2*ZoomY)+5));
}


int CVICALLBACK cbwin_Canvas(int panelHandle, int message, unsigned int* wParam, 
					unsigned int* lParam, void* callbackData) {
	switch (message) {
		case EVENT_NEWHANDLE: ;
	}
	return 0;
}

// Returns the minimum of 4 values
double MIN4(double a, double b, double c, double d) {
	if (         a<=b and a<=c and a<=d) return a;
	if (b<=a          and b<=c and b<=d) return b;
	if (c<=a and c<=b          and c<=d) return c;
	if (d<=a and d<=b and d<=c         ) return d;
	return a;	
}


int CVICALLBACK cb_Canvas (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int X, Y, TrueX, TrueY, Xh, Yh;
	static int Change=-3;	// none
	int DetectionRadius=20*ZoomX;	// pixels
	int OldXc, OldYc, Xr, Yr;
	
	switch (event) {
		case EVENT_GOT_FOCUS: CanvasFocused=TRUE;  break;
		case EVENT_LOST_FOCUS:CanvasFocused=FALSE; break;
		
//		case EVENT_LEFT_CLICK:
		case WM_LBUTTONUP:		// Custom event
		case WM_LBUTTONDOWN:	// Custom event

			X=eventData2-cLeft;
			Y=eventData1-cTop;
			if (BmpIn==0 or Width==0 or Height==0) return 0;
			
//			Zoom=sqrt(Width*Width+Height*Height)/sqrt(cWidth*cWidth+cHeight*cHeight);
//			ZoomX=(double)Width/cWidth;
//			ZoomY=(double)Height/cHeight;
			TrueX=ZoomX*X;
			TrueY=ZoomY*Y;
			Xh=Width/2;
			Yh=Height/2;

			// If Mouse Down: Find Closest
			#define Dist(i) (X##i==-1 ? 1e30 : sqrt((TrueX-X##i)*(TrueX-X##i)+(TrueY-Y##i)*(TrueY-Y##i)))
//			if (Dist(h)<Radius/2) Change=-1;	// Set the center
//			else if (X0==-1 and X1==-1 and X2==-1) Change=0;
//			else if (X0==-1) Change=0;
//			else if (X1==-1) Change=1;
//			else if (X2==-1) Change=2;
//			else
			if (event==WM_LBUTTONDOWN) {
				Change=-3;	// None selected
				Xr=Xc+Radius*cos(Rotation)*0.75; Yr=Yc+Radius*sin(Rotation)*0.75;
					 if (Dist(0)<MIN4(         Dist(1), Dist(2), Dist(c), Dist(r)) and Dist(0)<DetectionRadius) Change=0;
				else if (Dist(1)<MIN4(Dist(0),          Dist(2), Dist(c), Dist(r)) and Dist(1)<DetectionRadius) Change=1;
				else if (Dist(2)<MIN4(Dist(0), Dist(1),          Dist(c), Dist(r)) and Dist(2)<DetectionRadius) Change=2;
				else if (Dist(c)<MIN4(Dist(0), Dist(1), Dist(2),          Dist(r)) and Dist(c)<DetectionRadius) Change=-1;
				else if (Dist(r)<MIN4(Dist(0), Dist(1), Dist(2), Dist(c)         ) and Dist(r)<DetectionRadius) Change=-2;
			} // otherwise, just remember it from last time
			
			switch (Change) {
				case 0: X0=TrueX; Y0=TrueY; break;
				case 1: X1=TrueX; Y1=TrueY; break;
				case 2: X2=TrueX; Y2=TrueY; break;
				case -1:OldXc=Xc; OldYc=Yc; Xc=TrueX; Yc=TrueY; /*X0=X1=X2=-1;*/ break;
				case -2:Rotation=atan2(TrueY-Yc, TrueX-Xc); break;
			}
			
			// If the corner points have moved, we adjust the center and the radius
			if ((Change==0 or Change==1 or Change==2) and X0!=-1 and X1!=-1 and X2!=-1) {
				Yc=0.5*( (X2*X2-X0*X0+Y2*Y2-Y0*Y0)*(X1-X0) - (X1*X1-X0*X0+Y1*Y1-Y0*Y0)*(X2-X0) )/
					   ( (Y2-Y0)*(X1-X0)-(Y1-Y0)*(X2-X0) );
				Xc=0.5*( X1*X1-X0*X0+Y1*Y1-Y0*Y0-2*Yc*(Y1-Y0) )/(X1-X0);
				Radius=sqrt((Xc-X0)*(Xc-X0)+(Yc-Y0)*(Yc-Y0));
			}
			
			// if center has moved, we adjust the corner points
			if (Change==-1) {
				X0+=Xc-OldXc;	Y0+=Yc-OldYc;
				X1+=Xc-OldXc;	Y1+=Yc-OldYc;
				X2+=Xc-OldXc;	Y2+=Yc-OldYc;
			}
			
			DrawCircle();
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help Canvas", "Move the border (red) points to adjust the position of the circle.\n"
						"You can also move the center (blue) point and adjust the radius.\n"
						"The thin lines/ellipses should follow the 'verticals' of the image");
			return 1;
	}
	return 0;
}

int CVICALLBACK cb_Radius (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	double OldRadius;
	switch (event) {
		case EVENT_COMMIT:
			OldRadius=Radius;
			GetCtrlVal(Pnl, FE_RADIUS, &Radius);
			X0=Xc+Radius/OldRadius*(X0-Xc); Y0=Yc+Radius/OldRadius*(Y0-Yc);
			X1=Xc+Radius/OldRadius*(X1-Xc); Y1=Yc+Radius/OldRadius*(Y1-Yc);
			X2=Xc+Radius/OldRadius*(X2-Xc); Y2=Yc+Radius/OldRadius*(Y2-Yc);
			DrawCircle();
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help Center", "Set the radius of the image circle");
			return 1;
	}
	return 0;
}

int CVICALLBACK cb_Rotation (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(Pnl, FE_ROTATION, &Rotation);
			Rotation*=DegToRad;
			DrawCircle();
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help Center", "Set the rotation reference of the image\n(to try to keep the horizon horizontal)");
			return 1;
	}
	return 0;
}

int CVICALLBACK cb_Center (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int OldXc, OldYc;
	switch (event) {
		case EVENT_COMMIT:
			OldXc=Xc; OldYc=Yc;
			Xc=Width/2;
			Yc=Height/2;
			X0+=Xc-OldXc; Y0+=Yc-OldYc;
			X1+=Xc-OldXc; Y1+=Yc-OldYc;
			X2+=Xc-OldXc; Y2+=Yc-OldYc;
			DrawCircle();
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help Center", "Center the image circle on the exact center of the image");
			return 1;
	}
	return 0;
}

int CVICALLBACK cb_Vertical (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(panel, control, &Vertical);
			DrawCircle();
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help Vertical", "Whether the camera was pointing horizontally or vertically");
			return 1;
	}
	return 0;
}
