/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/* Copyright (c) National Instruments 2006. All Rights Reserved.          */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  BE                              1       /* callback function: cbp_Panel */
#define  BE_LOAD                         2       /* callback function: cb_Load */
#define  BE_PASTE                        3       /* callback function: cb_PasteFromClipboad */
#define  BE_REVERSE                      4       /* callback function: cb_Reverse */
#define  BE_DEFORM                       5       /* callback function: cb_Deform */
#define  BE_COPY                         6       /* callback function: cb_CopyToClipboad */
#define  BE_SAVE                         7       /* callback function: cb_Save */
#define  BE_ROTATION                     8       /* callback function: cb_Rotation */
#define  BE_QUIT                         9       /* callback function: cb_Quit */
#define  BE_CANVAS                       10
#define  BE_TRANSPARENT                  11      /* callback function: cb_Canvas */
#define  BE_TEXTMSG                      12

#define  FE                              2       /* callback function: cbp_Panel */
#define  FE_LOAD                         2       /* callback function: cb_Load */
#define  FE_PASTE                        3       /* callback function: cb_PasteFromClipboad */
#define  FE_VERTICAL                     4       /* callback function: cb_Vertical */
#define  FE_DEFORM                       5       /* callback function: cb_Deform */
#define  FE_COPY                         6       /* callback function: cb_CopyToClipboad */
#define  FE_SAVE                         7       /* callback function: cb_Save */
#define  FE_RADIUS                       8       /* callback function: cb_Radius */
#define  FE_ROTATION                     9       /* callback function: cb_Rotation */
#define  FE_CENTER                       10      /* callback function: cb_Center */
#define  FE_QUIT                         11      /* callback function: cb_Quit */
#define  FE_CANVAS                       12
#define  FE_TRANSPARENT                  13      /* callback function: cb_Canvas */
#define  FE_TEXTMSG                      14

#define  PNL                             3       /* callback function: cbp_Panel */
#define  PNL_FOCAL                       2       /* callback function: cb_Focal */
#define  PNL_10LINE                      3       /* callback function: cb_10Line */
#define  PNL_PCD                         4       /* callback function: cb_Pcd */
#define  PNL_RESOLUTION                  5       /* callback function: cd_Resol */
#define  PNL_DPI                         6       /* callback function: cb_Dpi */
#define  PNL_CANVAS                      7
#define  PNL_PSI                         8       /* callback function: cb_Psi */
#define  PNL_HORIZON                     9       /* callback function: cb_Horizon */
#define  PNL_DEFAULT_COLOR               10      /* callback function: cb_DefaultColor */
#define  PNL_WIDTH                       11      /* callback function: cb_WH */
#define  PNL_HEIGHT                      12      /* callback function: cb_WH */
#define  PNL_LENX                        13      /* callback function: cb_WH */
#define  PNL_LENY                        14      /* callback function: cb_WH */
#define  PNL_CANVAS_2                    15
#define  PNL_NEW_WIDTH                   16      /* callback function: cb_HW2 */
#define  PNL_NEW_HEIGHT                  17      /* callback function: cb_HW2 */
#define  PNL_ALPHAMAX                    18      /* callback function: cb_HW2 */
#define  PNL_PHIMAX                      19      /* callback function: cb_HW2 */
#define  PNL_PHIMIN                      20
#define  PNL_CANVAS_6                    21      /* callback function: cb_HW2 */
#define  PNL_FACTOR                      22      /* callback function: cb_Factor */
#define  PNL_LOAD                        23      /* callback function: cb_Load */
#define  PNL_SAVE                        24      /* callback function: cb_Save */
#define  PNL_PASTE                       25      /* callback function: cb_PasteFromClipboad */
#define  PNL_COPY                        26      /* callback function: cb_CopyToClipboad */
#define  PNL_QUIT                        27      /* callback function: cb_Quit */
#define  PNL_DEFORM                      28      /* callback function: cb_Deform */
#define  PNL_CANVAS_4                    29
#define  PNL_TRUE_SIZE                   30      /* callback function: cb_TrueSize */
#define  PNL_HORIZ                       31      /* callback function: cb_Horizon */
#define  PNL_PIC                         32
#define  PNL_HELP                        33      /* callback function: cb_Help */
#define  PNL_COPYRIGHT                   34


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */ 

int  CVICALLBACK cb_10Line(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Canvas(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Center(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_CopyToClipboad(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_DefaultColor(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Deform(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Dpi(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Factor(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Focal(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Help(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Horizon(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_HW2(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Load(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_PasteFromClipboad(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Pcd(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Psi(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Quit(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Radius(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Reverse(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Rotation(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Save(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_TrueSize(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Vertical(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_WH(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cbp_Panel(int panel, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cd_Resol(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
