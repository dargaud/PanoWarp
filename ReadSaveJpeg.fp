s��         /V     *N     �   ����                               Jpg                             ReadSaveJpeg                                                                                             
�  	png_text[]     � ��png_text  �  � ��BOOL  �    ReadSaveJpeg for LabWindows/CVI
(c) Guillaume Dargaud - Free use and redistribution.
    Can be integrated in a commercial program,
    but must not be sold as a fp/lib/dll or source.

History:
5/5/2001 ReadSaveJpeg functions
10/5/2001 Use of Bitmap when reading now optional
----------------------------------------------------------------
 
Save and read JPEG files.

      Two basic pairs of functions are provided, 
      one pair reads from a file to an empty bitmap or control;
      the other pair saves a bitmap or control to a file.
      
      JPEG files are lossy by nature and work well with photographic images
      but not well at all with non aliased text or graphics.
      For this is much better to use PNG (see my other ReadSavePng.fp)
      
      Files are 24 bits RGB or 8 bits greyscale.
      If you use a 32 bit mode, the bitmap will be converted to 24 bits.
      
      For display you must pass a handle of a control that can accept bitmaps,
      such as: picture controls / picture rings / picture buttons / graph controls
      
      Based on JPEG-6B by the Independent JPEG Group: http://www.ijg.org/

----------------------------------------------------------------
For source code or assistance contact me at:
dargaud@sung3.ifsi.rm.cnr.it
     ?    Saves a bitmap already in memory into a compressed JPEG file.        The array which is filled with the bits that determine the colors to be displayed on each pixel of the image.

This array is returned by the GetBitmapData() function.

The number of bits in the image for each pixel is equal to
the Pixel Depth.  The currently supported pixel depth is 8, the bits are the RGB values.  The color thereby determined is displayed at the location of that pixel.  

The first pixel in the array is at the top, left corner of the image.  The pixels in the array are in row-major order.

If you are using a 8-bit PixelDepth (greyscale image), each pixel is represented by a byte (0=black, 0xFF=white). In that case, RowBytes=Width.

If you are using a 24-bit Pixel Depth, each pixel is represented by a 24-bit RGB value of the form 0xRRGGBB, where RR,GG, and BB represent the red, green and blue intensity of the color.  The RR byte should always be at the lowest memory address of the three bytes.

If you are using a 32-bit Pixel Depth, each pixel is represented by a 32-bit RGB value of the form 0x00RRGGBB, where RR,GG, and BB represent the red, green and blue intensity of the color.  The 32-bit value is treated as a native 32-bit integer value for the platform.  The most significant byte is always ignored.  The BB byte should always be in the least significant byte.  On a little-endian platform (for example, Intel processors), BB would be at the lowest memory address.  
On a big-endian platform (for example, Motorola processors), BB would be at the highest address.  Note that this is different from the 24-bit Pixel Depth.


     �    Number of bytes on each scan line of the image.
Should be equal to 
 - 3*Width/PixDepth in 24 bit mode 
 - 4*Width/PixDepth in 32 bit mode.
 - Width in 8 bit mode (greyscale)
 - Width/8 in B/W mode

It is returned by the GetBitmapData() function.     �    Number of bits per pixel . Should be:
- 32 in 32-bit mode
- 24 in 24 bit mode
- 8 for greyscale images
- 1 for B&W mode (8 pixels per byte)

It is returned by the GetBitmapData() function.     Q    Width of the image, in pixels.

It is returned by the GetBitmapData() function.     R    Height of the image, in pixels.

It is returned by the GetBitmapData() function.     >    Name of the JPEG file to save. Should have extension ".jpg".     #    Return TRUE if an error happened.    h    Quality of the compression:
- 0: _very_ strong compression, highly destructive.
- 75: default compression
- 100: no compression (may be a bigger file than the equivalent PNG)

Remember that JPEG files have lossy compression, which means that you should never open them up, do a modification and save again, since the overall quality will decrease each time.
    S $ (          Bits                              y $ �          RowBytes                          y $          PixDepth                          ? _ (          Width                             � _ �          Height                            � � (    �    FileName                          8 �z���       Error                             c �          Quality                            0    0    0    0    0    ""    	            75    9    Saves a panel or a control into a compressed JPEG file.     @    Panel to save (if Control=0), or panel of the control to save.     :    Control to save (0 if you want to save the whole panel).     >    Name of the JPEG file to save. Should have extension ".jpg".     #    Return TRUE if an error happened.    h    Quality of the compression:
- 0: _very_ strong compression, highly destructive.
- 75: default compression
- 100: no compression (may be a bigger file than the equivalent PNG)

Remember that JPEG files have lossy compression, which means that you should never open them up, do a modification and save again, since the overall quality will decrease each time.
    � $            Panel                             5 $ �          Control                           w V          FileName                          � �����       Error                             � V �          Quality                            0    0    ""    	            75   H    Read a bitmap from a file.
This function can read both kind of jpeg file:
- 3*8 bit RGB
- 8 bits greyscale

BUT, it will convert it to a 24 bit RGB image internally.
I do not think it will read 12 bits JPG (but nobody uses those).

You must free(Bits) and DiscardBitmap(Bitmap) after you are done using the (optional) bitmap.
     >    Name of the JPEG file to save. Should have extension ".jpg".     #    Return TRUE if an error happened.     �    Pointer to a free bitmap.
After you are done using it, you must DiscardBitmap(Bitmap).

If you pass null, then the bitmap is not allocated but you still get the Bits array and its parameters, which may be enough in some cases.
     v    Empty array of bytes, passed by reference, containing the pixel information.
You must free(Bits) after you are done.     `    Return the number of bytes on each scan line of the image.
Should be equal to 8*Width/PixDepth     B    Number of bits per pixel will return 8 (greyscale) or 24 (RGB).
          Width of the image, in pixels.     !    Height of the image, in pixels.    � �          FileName                          p���       Error                             D $            Bitmap                            1 $ �         Bits                              � $ �          RowBytes                           $r          PixDepth                          a j           Width                             � j �          Height                             ""    	            	            	            	            	            	            	            �    Read a file's properties without loading the bitmap:
- size (width and height)
- number of color channels (1 or 3)
- bytes per row
- bits per pixel
     >    Name of the JPEG file to save. Should have extension ".jpg".     #    Return TRUE if an error happened.     `    Return the number of bytes on each scan line of the image.
Should be equal to 8*Width/PixDepth     B    Number of bits per pixel will return 8 (greyscale) or 24 (RGB).
          Width of the image, in pixels.     !    Height of the image, in pixels.     B    Number of bits per pixel will return 8 (greyscale) or 24 (RGB).
    R �          FileName                          �p���       Error                             � %            RowBytes                          + % �          PixDepth                          u j           Width                             � j �          Height                            � % �          NbChan                             ""    	            	            	            	            	            	            �    Read an image in a file and display it inside a control (which must be one of picture controls / picture rings / picture buttons / graph controls).     %    Panel where is located the control.     �    Control to read from the file.
It must be one of: 
    - picture controls
    - picture rings
    - picture buttons
    - graph controls     >    Name of the JPEG file to read. Should have extension ".jpg".     #    Return TRUE if an error happened.     �    For picture rings, this argument is the zero-based index of a particular image in the ring.

For graphs, this argument is the plotHandle returned from PlotBitmap.

For picture controls and picture buttons, this argument is ignored.    !n 8            Panel                             !� 8 �          Control                           "- y          FileName                          "s �����       Error                             "� 8          ImageID                            0    0    ""    	            0   R    Transform a RGB image to a greyscale one (one pixel per byte).

Ratio is one byte out of 3 in 24-bit RGB mode, and one out of 4 in 32-bit mode. So we divide the image size by 3 or 4.

NOTE: we use the Green channel as reference, if red or blue are different, then screw it !
The transformation can be done in place, that is Dest=Source
     %    Destination array of NbPixels bytes     &    Source array of NbPixels*Ratio bytes     ?    Number of pixels (not bytes) to copy.  (=Width*Height pixels)     c    One byte out of 3 in 24 bit mode,
one byte out of 4 or 32 bit mode.

(=PixDepth/8=RowBytes/Width)    & $           Dest                              &@ $ �         Source                            &n ~           NbPixels                          &� ~ �          Ratio                              	            0    0    0    �    Transform a 32 bit RGB image to a 24 bit one.
JPG file only accept 24 bits (or 8 bits greyscale).
If you pass a 32 bits array, the Jpg_Save... routines will convert it.

The transformation can be done in place (that is Dest=Source)     )    Destination array of NbPixels*3/4 bytes     &    Source array of Width*Height*4 bytes     2    Number of bytes to copy.  (=Width*Height pixels)    )  $ !          Dest                              )1 $ �         Source                            )_ $L          NbPixels                           	            0    0 ����           �             K.        SaveBitmapToFile                                                                                                                        ����         �  X             K.        SavePanelControlToFile                                                                                                                  ����         �  �             K.        ReadBitmapFromFile                                                                                                                      ����         �               K.        GetProperties                                                                                                                           ����          �  #�             K.        ReadControlFromFile                                                                                                                     ����         $�  '              K.        CopyRGBtoGray                                                                                                                           ����         (  )�             K.        CopyRGB32to24                                                                                                                                                                                                                      �Save Bitmap as JPEG                                                                  �Save panel or control as JPEG                                                        �Read Bitmap From JPEG File                                                           �Get image Properties                                                                 �Read Control From JPEG File                                                          �RBG to Greyscale image conversion                                                    �RGB 32 to 24 bits image conversion                                              